<?php
/**
 *Description
 *@filename config.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */

//starting the session
  session_start();
  ob_start();

//checking if the session is set with the predefined csrf, if not making it empty
  if(empty($_SESSION['site_csrf'])){
     $_SESSION['site_csrf'] = md5(time());
  }
  $site_csrf = $_SESSION['site_csrf'];
  
//setting up the csrf token for the website
 
  
// Displaying errors.
  ini_set('display_errors', 1);
  ini_set('error_reporting', E_ALL);
  
// Database credentials.
  define('DB_USER', 'root');
  define('DB_PASS', '');
  define('DB_DSN', 'mysql:host=localhost;dbname=gaming_db');
  
// Database handler
  $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
  $dbh->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

//base path(current directory)
  define('APP', __DIR__);

//function to autoload class on every page.
function autoload($className)
{
  $className = ltrim($className, '\\');
  $fileName  = '';
  $namespace = '';
  if ($lastNsPos = strrpos($className, '\\')) {
    $namespace = substr($className, 0, $lastNsPos);
    $className = substr($className, $lastNsPos + 1);
    $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
  }
  $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';
  
  require $fileName;
}
spl_autoload_register('autoload');