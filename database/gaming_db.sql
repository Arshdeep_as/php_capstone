-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Sep 15, 2018 at 09:41 PM
-- Server version: 10.2.12-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gaming_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `deleted_product`
--

DROP TABLE IF EXISTS `deleted_product`;
CREATE TABLE IF NOT EXISTS `deleted_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `product_sku` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `developer` varchar(200) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `deleted_at` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `deleted_product`
--

INSERT INTO `deleted_product` (`id`, `product_id`, `product_sku`, `name`, `publisher`, `developer`, `price`, `image`, `thumbnail`, `in_stock`, `quantity`, `description`, `rating_id`, `supplier_id`, `deleted_at`) VALUES
(23, 21, 123132, 'Alpha', 'Arshdeep', 'Rangi', 33.9, 'aplhapicture', 'alpha.jpg', 1, 100, 'this is the best game i played', 3, 3, '2018-09-12 12:36:05'),
(24, 23, 666666, 'aaa', 'aaa', 'aaaa', 23, 'aaaa', 'qqq', 1, 43, 'ssss', 3, 7, '2018-09-12 12:45:51'),
(25, 24, 1213213, 'zXCsdc', 'hjyuguhjg', 'hgjhj', 98, 'bjkn', 'bkjbkj', 1, 76878, 'nnnjbjbk', 2, 9, '2018-09-12 15:09:13'),
(26, 1, 0, 'GTA 55', 'Rockstar Games', 'Rockstar north', 69.56, 'gta5', 'gta5', 1, 100, 'Grand Theft Auto V is an action-adventure video game developed by Rockstar North and published by Rockstar Games. It was released in September 2013 for PlayStation 3 and Xbox 360, in November 2014 for PlayStation 4 and Xbox One, and in April 2015 for Microsoft Windows. It is the first main entry in the Grand Theft Auto series since 2008\'s Grand Theft Auto IV. Set within the fictional state of San Andreas, based on Southern California, the single-player story follows three criminals and their efforts to commit heists while under pressure from a government agency. The open world design lets players freely roam San Andreas\' open countryside and the fictional city of Los Santos, based on Los Angeles', 1, 1, '2018-09-12 15:39:25'),
(27, 25, 12234, 'gggg', 'hhhhh', 'ggggg', 98.99, 'hhh', 'hhh', 1, 89, 'jhjhj', 4, 9, '2018-09-12 15:54:31'),
(28, 26, 123456, 'qwerty123', 'qwerty123', 'qwerty', 89, 'jhgfgh', 'jhhghh', 1, 89, 'hjhghjhgg', 2, 9, '2018-09-13 10:12:55'),
(29, 28, 1232321, 'hhhhhh', 'jjjjj', 'kkkkk', 8888, 'jjjjj', 'jjjjj', 1, 89, '00', 2, 9, '2018-09-13 11:52:38'),
(30, 29, 3333, 'nav uuhhggf', 'kjenfkej', 'jknkaj', 89, 'njh', 'nkjn', 1, 21, 'wadwdwa', 2, 4, '2018-09-13 12:52:55'),
(31, 30, 234321, 'jjjjkkklll', 'hhhh', 'hhhh', 34.23, 'hhh', 'hhhhh', 1, 123, 'hhhh', 1, 8, '2018-09-15 12:40:30');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`genre_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Action', '2018-09-05 16:45:53', NULL),
(2, 'Action', '2018-09-05 16:45:53', NULL),
(3, 'Adventure', '2018-09-05 16:45:53', NULL),
(4, 'Shooting', '2018-09-05 16:45:53', NULL),
(5, 'Survival', '2018-09-05 16:45:53', NULL),
(6, 'Racing', '2018-09-05 16:45:53', NULL),
(7, 'Stleath', '2018-09-05 16:45:53', NULL),
(8, 'Puzzle', '2018-09-05 16:45:53', NULL),
(9, 'Sports', '2018-09-05 16:45:53', NULL),
(10, 'Role Playing', '2018-09-05 16:45:53', NULL),
(11, 'First person shooter', '2018-09-05 16:45:53', NULL),
(12, 'Third person shooter', '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice`
--

DROP TABLE IF EXISTS `invoice`;
CREATE TABLE IF NOT EXISTS `invoice` (
  `invoice_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(11) NOT NULL,
  `invoice_date` varchar(255) DEFAULT NULL,
  `sub_total` varchar(255) NOT NULL,
  `gst` varchar(255) NOT NULL,
  `pst` varchar(255) NOT NULL,
  `total` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`invoice_id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice`
--

INSERT INTO `invoice` (`invoice_id`, `user_id`, `invoice_date`, `sub_total`, `gst`, `pst`, `total`, `created_at`, `updated_at`) VALUES
(1, ':user_id', '2018-09-14 13:57:25', ':sub_total', ':gst', ':pst', ':total', '2018-09-14 13:57:25', NULL),
(2, ':user_id', '2018-09-14 14:07:30', ':sub_total', ':gst', ':pst', ':total', '2018-09-14 14:07:30', NULL),
(3, ':user_id', '2018-09-14 14:13:44', ':sub_total', ':gst', ':pst', ':total', '2018-09-14 14:13:44', NULL),
(4, ':user_id', '2018-09-14 14:20:15', ':sub_total', ':gst', ':pst', ':total', '2018-09-14 14:20:15', NULL),
(5, ':user_id', '2018-09-14 14:36:12', ':sub_total', ':gst', ':pst', ':total', '2018-09-14 14:36:12', NULL),
(6, '10', '2018-09-14 14:37:22', '66.7', '3.34', '5.34', '75.38', '2018-09-14 14:37:22', NULL),
(7, '10', '2018-09-14 14:50:48', '145.57', '7.28', '11.65', '164.5', '2018-09-14 14:50:48', NULL),
(8, '10', '2018-09-14 16:38:00', '224.44', '11.22', '17.96', '253.62', '2018-09-14 16:38:00', NULL),
(9, '16', '2018-09-14 17:06:32', '233.05', '11.65', '18.64', '263.34', '2018-09-14 17:06:32', NULL),
(10, '10', '2018-09-14 18:49:30', '155.96', '7.8', '12.48', '176.24', '2018-09-14 18:49:30', NULL),
(11, '10', '2018-09-14 19:14:52', '155.96', '7.8', '12.48', '176.24', '2018-09-14 19:14:52', NULL),
(12, '10', '2018-09-14 19:15:32', '155.96', '7.8', '12.48', '176.24', '2018-09-14 19:15:32', NULL),
(13, '10', '2018-09-14 19:15:58', '155.96', '7.8', '12.48', '176.24', '2018-09-14 19:15:58', NULL),
(14, '10', '2018-09-14 19:16:48', '155.96', '7.8', '12.48', '176.24', '2018-09-14 19:16:48', NULL),
(15, '10', '2018-09-14 19:23:56', '67.94', '3.4', '5.44', '76.78', '2018-09-14 19:23:56', NULL),
(16, '10', '2018-09-15 09:18:57', '46.96', '2.35', '3.76', '53.07', '2018-09-15 09:18:57', NULL),
(17, '10', '2018-09-15 09:21:01', '78.87', '3.94', '6.31', '89.12', '2018-09-15 09:21:01', NULL),
(18, '10', '2018-09-15 09:36:40', '78.87', '3.94', '6.31', '89.12', '2018-09-15 09:36:40', NULL),
(19, '10', '2018-09-15 09:50:28', '78.87', '3.94', '6.31', '89.12', '2018-09-15 09:50:28', NULL),
(20, '10', '2018-09-15 11:15:27', '78.87', '3.94', '6.31', '89.12', '2018-09-15 11:15:27', NULL),
(21, '10', '2018-09-15 13:07:31', '77.09', '3.85', '6.17', '87.11', '2018-09-15 13:07:31', NULL),
(22, '17', '2018-09-15 14:21:03', '77.09', '3.85', '6.17', '87.11', '2018-09-15 14:21:03', NULL),
(23, '17', '2018-09-15 14:23:12', '77.09', '3.85', '6.17', '87.11', '2018-09-15 14:23:12', NULL),
(24, '17', '2018-09-15 14:24:06', '55.09', '2.75', '4.41', '62.25', '2018-09-15 14:24:06', NULL),
(25, '17', '2018-09-15 14:24:40', '55.09', '2.75', '4.41', '62.25', '2018-09-15 14:24:40', NULL),
(26, '10', '2018-09-15 15:09:46', '77.09', '3.85', '6.17', '87.11', '2018-09-15 15:09:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `invoice_product`
--

DROP TABLE IF EXISTS `invoice_product`;
CREATE TABLE IF NOT EXISTS `invoice_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `invoice_product`
--

INSERT INTO `invoice_product` (`id`, `invoice_id`, `product_id`) VALUES
(1, 12, 5),
(2, 12, 4),
(3, 13, 5),
(4, 13, 4),
(5, 14, 5),
(6, 14, 4),
(7, 15, 10),
(8, 15, 17),
(9, 16, 10),
(10, 17, 4),
(11, 18, 4),
(12, 19, 4),
(13, 20, 4),
(14, 21, 5),
(15, 22, 5),
(16, 23, 5),
(17, 24, 8),
(18, 25, 8),
(19, 26, 5);

-- --------------------------------------------------------

--
-- Table structure for table `platform`
--

DROP TABLE IF EXISTS `platform`;
CREATE TABLE IF NOT EXISTS `platform` (
  `platform_id` int(11) NOT NULL AUTO_INCREMENT,
  `platform_name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`platform_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `platform`
--

INSERT INTO `platform` (`platform_id`, `platform_name`, `created_at`, `updated_at`) VALUES
(1, 'PC', '2018-09-05 16:45:53', NULL),
(2, 'Xbox One', '2018-09-05 16:45:53', NULL),
(3, 'PS 4', '2018-09-05 16:45:53', NULL),
(4, 'PS4 Pro', '2018-09-05 16:45:53', NULL),
(5, 'Xbox 360', '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_sku` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `developer` varchar(200) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `image` varchar(100) DEFAULT NULL,
  `thumbnail` varchar(50) DEFAULT NULL,
  `in_stock` tinyint(1) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `description` longtext DEFAULT NULL,
  `rating_id` int(11) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`product_id`, `product_sku`, `name`, `publisher`, `developer`, `price`, `image`, `thumbnail`, `in_stock`, `quantity`, `description`, `rating_id`, `supplier_id`, `created_at`, `updated_at`) VALUES
(2, 0, 'God of War', 'Sony Interactive', 'SIE Santa Monica', 89.99, 'god_of_war', 'god_of_war', 1, 100, 'God of War[a] is an action-adventure video game developed by Santa Monica Studio and published by Sony Interactive Entertainment (SIE). Released on April 20, 2018, for the PlayStation 4 (PS4) console, it is the eighth installment in the God of War series, the eighth chronologically, and the sequel to 2010\'s God of War III. Unlike previous games, which were loosely based on Greek mythology, this game is loosely based on Norse mythology. The main protagonists are Kratos, the former Greek God of War, and his young son Atreus. Following the death of Kratos\' second wife and Atreus\' mother, they journey to fulfill her promise and spread her ashes at the highest peak of the nine realms. Kratos keeps his troubled past a secret from Atreus, who is unaware of his divine nature. Along their journey, they encounter monsters and gods of the Norse world.', 4, 4, '2018-09-05 16:45:53', '2018-09-13 11:31:41'),
(3, 0, 'Halo 4', 'Microsoft Studios', '343 Industries', 56.45, 'halo4', 'halo4', 0, 50, 'Halo 4 is a first-person shooter developed by 343 Industries and published by Microsoft Studios for the Xbox 360 video game console. The seventh video game installment in the Halo franchise, the game was released in November 2012. Halo 4\'s story follows a cybernetically enhanced human supersoldier, Master Chief, and his artificial intelligence construct Cortana, as they encounter unknown threats while exploring an ancient civilization\'s planet. The player assumes the role of Master Chief, who battles remnants of the former military alliance of alien races known as the Covenant and mechanical warriors of the Forerunner empire known as the Prometheans. The game features a selection of weapons, enemies, and game modes not present in previous titles of the series.', 4, 5, '2018-09-05 16:45:53', NULL),
(4, 0, 'NFS Payback', 'Electronic Arts', 'Ghost Games', 78.87, 'nfs_payback', 'nfs_payback', 0, 100, 'Need for Speed Payback is a racing video game developed by Ghost Games and published by Electronic Arts for Microsoft Windows, PlayStation 4 and Xbox One. It is the twenty-third installment in the Need for Speed series. The game was revealed with a trailer released on June 2, 2017. It was released worldwide on November 10, 2017.', 2, 6, '2018-09-05 16:45:53', NULL),
(5, 0, 'For Honor', 'Ubisoft', 'Ubisoft Montreal', 77.09, 'for_honor', 'for_honor', 0, 60, 'For Honor is a video game developed and published by Ubisoft for Microsoft Windows, PlayStation 4, and Xbox One. The game allows players to play the roles of historical forms of soldiers and warriors, including knights, samurai, and vikings within a medieval setting, controlled using a third-person perspective. ', 4, 2, '2018-09-05 16:45:53', NULL),
(6, 0, 'AC Origins', 'Ubisoft', 'Ubisoft Montreal', 52.98, 'ac_origins', 'ac_origins', 0, 70, 'Assassin\'s Creed Origins is an action-adventure video game developed by Ubisoft Montreal and published by Ubisoft. It is the tenth major installment in the Assassin\'s Creed series and the successor to 2015\'s Assassin\'s Creed Syndicate. It was released worldwide for Microsoft Windows, PlayStation 4, and Xbox One on October 27, 2017. The game is set in Egypt near the end of the Ptolemaic period (49–47 BC) and recounts the secret fictional history of real-world events. The story follows a Medjay named Bayek, and explores the origins of the centuries-long conflict between the Brotherhood of Assassins, who fight for peace by promoting liberty, and The Order of the Ancients—forerunners to the Templar Order—who desire peace through the forced imposition of order', 4, 5, '2018-09-05 16:45:53', NULL),
(7, 0, 'NBA 2K18', '2K Sports', 'Visual Concepts', 27.62, 'nba_2k18', 'nba_2k18', 0, 60, 'NBA 2K18 is a basketball simulation video game developed by Visual Concepts and published by 2K Sports. It is the 19th installment in the NBA 2K franchise, the successor to NBA 2K17, and the predeccessor to NBA 2K19. It was released in September 2017 for Microsoft Windows, Nintendo Switch, PlayStation 4, PlayStation 3, iOS, Android, Xbox One, and Xbox 360. Kyrie Irving of the Boston Celtics serves as cover athlete for the regular edition of the game, Shaquille O\'Neal is the cover athlete for the special editions, and DeMar DeRozan of the Toronto Raptors is the cover athlete for the game in Canada. While a member of the Cleveland Cavaliers when selected for the cover, Irving was traded to the Boston Celtics prior to the game\'s release. As a result, a new cover depicting Irving in a Celtics uniform was revealed alongside the original cover.', 2, 1, '2018-09-05 16:45:53', NULL),
(8, 0, 'WWE 2K18', '2K Games', 'Visual Concepts', 55.09, 'wwe_2k18', 'wwe_2k18', 0, 50, 'WWE 2K18 is a professional wrestling video game developed by Yuke\'s and Visual Concepts, and published by 2K Sports. It is the nineteenth installment in the WWE game series (fifth under the WWE 2K banner) and a follow-up to WWE 2K17. It was released worldwide on October 17, 2017 for PlayStation 4, Xbox One and Microsoft Windows. With its release, WWE 2K18 became the first in the series to be exclusively released on eighth generation hardware and also the first in the series to be released for PC and consoles simultaneously. A Nintendo Switch version followed on December 6, 2017, the inaugural release of the series for the Switch and the first WWE game to be released for a Nintendo platform since WWE \'13', 2, 3, '2018-09-05 16:45:53', '2018-09-15 13:26:46'),
(9, 0, 'Far Cry 5', 'Ubisoft', 'Ubisoft Montreal', 65.39, 'farcry5', 'farcry5', 1, 60, 'Welcome to Hope County, Montana, land of the free and the brave, but also home to a fanatical doomsday cult known as Eden\'s Gate. Stand up to the cult\'s leader, Joseph Seed, his siblings, the Heralds, and spark the fires of resistance that will liberate your besieged community. From the savage mountain forest to the hamlet of Fall\'s End, freely explore Montana\'s rivers, lands, and skies in true Far Cry style, with more customizable weapons and vehicles than ever before. Carve your own path through a world that reacts to your decisions. Find your own way to survive, thrive, and beat back the forces of oppression. The stakes have never been higher. ', 1, 6, '2018-09-05 16:45:53', '2018-09-12 08:36:13'),
(10, 0, 'The Witcher 3', 'CD Projekt', 'CD Projekt Red', 46.96, 'witcher', 'witcher', 0, 50, 'The Witcher 3: Wild Hunt is a 2015 action role-playing video game developed and published by CD Projekt. Based on The Witcher series of fantasy novels by Polish author Andrzej Sapkowski, it is the sequel to the 2011 game The Witcher 2: Assassins of Kings. Played in an open world with a third-person perspective, players control protagonist Geralt of Rivia, a monster hunter known as a Witcher, who is looking for his missing adopted daughter on the run from the Wild Hunt: an otherworldly force determined to capture and use her powers. Players battle the game\'s many dangers with weapons and magic, interact with non-player characters, and complete main-story and side quests to acquire experience points and gold, which are used to increase Geralt\'s abilities and purchase equipment. Its central story has several endings, determined by the player\'s choices at certain points in the game. ', 1, 4, '2018-09-05 16:45:53', NULL),
(11, 0, 'Max Payne 3', 'Rockstar Games', 'Rockstar Studios', 16.83, 'maxpayne', 'maxpayne', 0, 60, 'Max Payne 3 is a third-person shooter video game developed by Rockstar Studios and published by Rockstar Games. It was released on May 15, 2012 for the PlayStation 3 and Xbox 360; a Microsoft Windows port was released on May 29, 2012, followed by an OS X port on June 20, 2013. The game is the first entry in the Max Payne series since 2003\'s Max Payne 2: The Fall of Max Payne, developed by Remedy Entertainment.\r\n\r\nThe game is played from a third-person perspective. Throughout the single-player mode, players control Max Payne, a former detective who has become a vigilante after the murder of his wife and daughter. Nine years after the events of the second game, Max becomes employed as a private security contractor in Brazil, but quickly becomes entangled in a quest filled with death and betrayal. An online multiplayer mode is included with the game, allowing up to 16 players to engage in both cooperative and competitive gameplay in re-creations of multiple single-player settings. ', 2, 1, '2018-09-05 16:45:53', NULL),
(12, 0, 'Fallout 4', 'Bethesda Softworks', 'Bethesda Game Studios', 27.74, 'fallout', 'fallout', 0, 70, 'Fallout 4 is a post-apocalyptic action role-playing video game developed by Bethesda Game Studios and published by Bethesda Softworks. It is the fifth major installment in the Fallout series and was released worldwide on November 10, 2015, for Microsoft Windows, PlayStation 4 and Xbox One. The game is set within an open world post-apocalyptic environment that encompasses the city of Boston and the surrounding Massachusetts region known as \"The Commonwealth\". The main story takes place in the year 2287, ten years after the events of Fallout 3 and 210 years after \"The Great War\", which caused catastrophic nuclear devastation across the United States.\r\n\r\nThe player assumes control of a character referred to as the \"Sole Survivor\", who emerges from a long-term cryogenic stasis in Vault 111, an underground nuclear fallout shelter. After witnessing the murder of their spouse and kidnapping of their son, the Sole Survivor ventures out into the Commonwealth to search for their missing child. The player explores the game\'s dilapidated world, complete various quests, help out factions, and acquire experience points to level up and increase the abilities of their character. New features to the series include the ability to develop and manage settlements, and an extensive crafting system where materials scavenged from the environment can be used to craft drugs and explosives, upgrade weapons and armor, and construct, furnish and improve settlements. Fallout 4 also marks the first game in the series to feature full voice acting for the protagonist. ', 4, 5, '2018-09-05 16:45:53', NULL),
(13, 0, 'Uncharted 4', 'Sony Computer Entertainment', 'Naughty Dog', 49.87, 'uncharted4', 'uncharted4', 0, 100, 'Uncharted 4: A Thief\'s End is an action-adventure game developed by Naughty Dog and published by Sony Computer Entertainment for PlayStation 4 in May 2016. Following Uncharted 3: Drake\'s Deception, it is the final Uncharted game to feature protagonist Nathan Drake (portrayed by Nolan North). Drake, retired from fortune hunting with his wife Elena, reunites with his estranged older brother Sam and longtime partner Sully to search for Captain Henry Avery\'s lost treasure.\r\n\r\nThe game features larger and more open levels than previous Uncharted games; players have more freedom to explore and move in combat, progress through enemy encounters undetected using stealth, and can use vehicles. Sony revealed Uncharted 4 at the North America PlayStation 4 launch event in November 2013. Development was hampered by extensive script changes and reshoots following the departures of lead Uncharted writer and creative director Amy Hennig and director Justin Richmond in March 2014. Bruce Straley and Neil Druckmann, who had directed Naughty Dog\'s previous game, The Last of Us, became the directors. ', 3, 7, '2018-09-05 16:45:53', NULL),
(14, 0, 'Bloodborne', 'Sony Computer Entertainment', 'FromSoftware', 27.89, 'bloodborne', 'bloodborne', 0, 50, 'Bloodborne is an action role-playing game developed by FromSoftware and published by Sony Computer Entertainment for PlayStation 4. Announced at Sony\'s E3 2014 conference, the game was released worldwide in March 2015. Bloodborne follows the player character, the Hunter, through the decrepit Gothic, Victorian era inspired city of Yharnam, whose inhabitants have been afflicted with an abnormal blood-borne disease, with the player character unraveling the city\'s intriguing mysteries while fighting beasts, ultimately attempting to find the source of the plague and stop it.\r\n\r\nThe game is played from a third-person perspective, players control a customizable protagonist, and gameplay is focused on weapons-based combat and exploration. Players battle varied enemies, including bosses, using items such as swords and firearms, and journey through the story, exploring the game\'s different locations, interacting with non-player characters, collecting key items involved in the story, and discovering and unraveling the world\'s many mysteries. Bloodborne began development in 2012 under the working title of Project Beast. Bearing many similarities to the Souls series of games by the same company, Bloodborne was partially inspired by the literary works of authors H. P. Lovecraft and Bram Stoker, and the architectural design of certain real world locations in places such as Romania and the Czech Republic. The decision by game director Hidetaka Miyazaki to create a new intellectual property (IP) and not another Souls game was made because he wanted to create something \"different\"; at the same time, Sony wanted a new IP to be made exclusively for the PlayStation 4. ', 2, 4, '2018-09-05 16:45:53', NULL),
(15, 0, 'Dishonored 2', 'Bethesda Softwares', 'Arkane Studios', 45.98, 'dishonored2', 'dishonored2', 0, 60, 'Dishonored 2 is an action-adventure video game developed by Arkane Studios and published by Bethesda Softworks. The sequel to 2012\'s Dishonored, the game was released for Microsoft Windows, PlayStation 4, and Xbox One on 11 November 2016. The game takes place in the fictional city of Karnaca. After Empress Emily Kaldwin is deposed by the witch Delilah Copperspoon, the player may choose between playing as either Emily or her Royal Protector and father Corvo Attano as they attempt to reclaim the throne. Both Emily and Corvo employ their own array of supernatural abilities, though the player can alternatively decide to forfeit these abilities altogether. There is a multitude of ways to complete missions, from stealth to purposeful violent conflict, navigated through a sandbox environment.\r\n\r\nIdeas for Dishonored 2 began while developing the downloadable content of its predecessor, which spawned the decision to create a voice for Corvo Attano after being a silent character in the first installment. The advancement of the timeline was brought about once Emily Kaldwin, only a child in Dishonored, was first proposed as a playable character. The game\'s aesthetic was influenced by paintings and sculptures. Set in the new fictional city of Karnaca, its history was invented over the course of one year. The city itself was based on southern European countries like Greece, Italy, and Spain, drawing on the architecture, fashion, and technologies of 1851. ', 3, 2, '2018-09-05 16:45:53', NULL),
(16, 0, 'Arkham City', 'Warner Bros. Interactive Entertainment', 'Rocksteady Studios', 48.87, 'arkhamcity', 'arkhamcity', 0, 50, 'Batman: Arkham City is a 2011 action-adventure video game developed by Rocksteady Studios and published by Warner Bros. Interactive Entertainment. Based on the DC Comics superhero Batman, it is the sequel to the 2009 video game Batman: Arkham Asylum and the second installment in the Batman: Arkham series. Written by veteran Batman writer Paul Dini with Paul Crocker and Sefton Hill, Arkham City is inspired by the long-running comic book mythos. In the game\'s main storyline, Batman is incarcerated in Arkham City, a huge new super-prison enclosing the decaying urban slums of fictional Gotham City. He must uncover the secret behind the sinister scheme, \"Protocol 10\", orchestrated by the facility\'s warden, Hugo Strange. The game\'s leading characters are predominantly voiced by actors from the DC Animated Universe, with Kevin Conroy and Mark Hamill reprising their roles as Batman and the Joker, respectively.\r\n\r\nThe game is presented from the third-person perspective with a primary focus on Batman\'s combat and stealth abilities, detective skills, and gadgets that can be used in both combat and exploration. Batman can freely move around the Arkham City prison, interacting with characters and undertaking missions, and unlocking new areas by progressing through the main story or obtaining new equipment. The player is able to complete side missions away from the main story to unlock additional content and collectible items. Arkham City gives Batman the new ability to glide using his cape, enabling him to cover greater distances than were possible in Arkham Asylum. Batman\'s ally Catwoman is another playable character, featuring her own story campaign that runs parallel to the game\'s main plot. ', 3, 5, '2018-09-05 16:45:53', NULL),
(17, 0, 'Black Flag', 'Ubisoft', 'Ubisoft Montreal', 20.98, 'blackflag', 'blackflag', 0, 40, 'Assassin\'s Creed IV: Black Flag is an action-adventure video game developed by Ubisoft Montreal and published by Ubisoft. It is the sixth major installment in the Assassin\'s Creed series. Its historical time frame precedes that of Assassin\'s Creed III (2012), though its modern-day sequences succeed III\'s own. Black Flag was first released for PlayStation 3, Xbox 360, and Nintendo Wii U in October 2013 and a month later for PlayStation 4, Xbox One, and Microsoft Windows.\r\n\r\nThe plot is set in a fictional history of real world events and follows the centuries-old struggle between the Assassins, who fight for peace with free will, and the Templars, who desire peace through control. The framing story is set in the 21st century and describes the player as an Abstergo agent. The main story is set in the 18th century Caribbean during the Golden Age of Piracy, and follows notorious Welsh pirate Edward Kenway, grandfather and father of Assassin\'s Creed III protagonist and antagonist Ratonhnhaké:ton and Haytham Kenway respectively, who stumbles upon the Assassin/Templar conflict. The attempted establishment of a Republic of Pirates utopia (free from either British or Spanish rule) is a significant plot element. ', 1, 6, '2018-09-05 16:45:53', NULL),
(18, 0, 'Mad Max', 'Warner Bros. Interactive Entertainment', 'Avalanche Studios', 22.43, 'madmax', 'madmax', 0, 30, 'Mad Max is an action-adventure video game based on the Mad Max franchise. Developed by Avalanche Studios and published by Warner Bros. Interactive Entertainment, it was released for Microsoft Windows, PlayStation 4, and Xbox One in September 2015. Feral Interactive published the game\'s macOS and Linux versions. In the game, players control Max Rockatansky as he progresses through the wasteland building a vehicle, his \"Magnum Opus\", to do battle with a gang of raiders, led by Scabrous Scrotus, and to reach the storied \"Plains of Silence\", where he hopes to find peace. Mad Max emphasizes vehicular combat, in which players can use weapon and armor upgrades on their car to fight enemies. It is set in an open post-apocalyptic wasteland consisting of deserts, canyons, and caves.\r\n\r\nTwo other Mad Max games, developed by Cory Barlog and Interplay Entertainment respectively, were in production before the announcement of this game, but neither of them were successfully released. Although Mad Max is not based on the film series, it was inspired by its universe, and franchise creator George Miller was consulted during the game\'s pre-production. Avalanche Studios found developing a vehicular-combat video game a challenge because of their inexperience with creating that type of game. Announced at the 2013 Electronic Entertainment Expo, the game was re-tooled during development and the PlayStation 3 and Xbox 360 versions were canceled. ', 3, 7, '2018-09-05 16:45:53', NULL),
(19, 0, 'Titanfall 2', 'Electronic Arts', 'Respawn Entertainment', 34.43, 'titanfall2', 'titanfall2', 0, 40, 'Titanfall 2 is a first-person shooter video game, developed by Respawn Entertainment and published by Electronic Arts. A sequel to 2014\'s Titanfall, the game was released worldwide on October 28, 2016 for Microsoft Windows, PlayStation 4 and Xbox One. In Titanfall 2, players control Titans, mecha-style exoskeletons and their pilots, who are agile and equipped with a variety of skills ranging from wall-running to cloaking. Set in a science fiction universe, the single-player campaign follows the story of Jack Cooper, a rifleman from Frontier Militia, who bonds with his Titan BT-7274 after an accident. Together, they embark on a quest to stop the Interstellar Manufacturing Corporation (IMC) from launching a superweapon.\r\n\r\nThe game\'s development began in mid-2014, and the title had a two-year development cycle. The decision to add a single-player campaign to the game came about because the team wanted to expand the game\'s player base. They came up with different ideas and prototypes, and integrated them to form a single coherent campaign. Gargantia on the Verdurous Planet and buddy cop films, as well as the video game Half-Life inspired the game\'s campaign and narrative. The team also overhauled the progression system and made subtle changes to the multiplayer to make the game fairer to players. Valve Corporation\'s Source engine powers the game. Stephen Barton returned to compose the game\'s music. ', 3, 1, '2018-09-05 16:45:53', NULL),
(20, 0, 'Need for Speed', 'Electronic Arts', 'Ghost Games', 34.43, 'needforspeed', 'needforspeed', 0, 50, 'Need for Speed is an online open world racing video game developed by Ghost Games and published by Electronic Arts, released for PlayStation 4 and Xbox One in November 2015, and released on Origin for Microsoft Windows on March 15, 2016. It is the twenty-second installment in the Need for Speed series, and is a reboot of the franchise. It marks the series\' second eighth generation installment.\r\nThe story revolves around the player and a small ragtag group of racers waiting to be noticed by any of the game\'s five icons, all of them being real-world motorsport and street racing figures. Spike (Adam Long) wants to meet Magnus Walker, Amy (Faye Marsay) has a desire to greet Akira Nakai, Robyn (Christina Wolfe) wants to come to Risky Devil, Manu (Howard Charles) wants to impress Ken Block, and Travis (Leo Gregory) is inspired by Shinichi Morohoshi. As the game progresses, the player earns money and reputation among the other drivers and people he meets along the way. As soon as the player defeats Magnus Walker, however, a cutscene appears in which Spike becomes jealous and angry, saying that the player got the chance before Spike did. After this, Spike gets over it because of Travis saying to him that if one of them gets noticed, they all get noticed. Once the player becomes the ultimate icon, the final challenge is against Travis, Spike, Amy, Robyn, Manu, and the icons. After the race, the final cutscene includes all of them taking a group photo together, with the player wearing a mask to hide his true identity. ', 2, 4, '2018-09-05 16:45:53', NULL),
(27, 123456, 'hjerewrbsdeaaaa', 'hbhjhj', 'hjbhjbhj', 9.09, 'jknjjahuidgyu', 'kjnkj', 1, 7, 'hvhvh', 3, 9, '2018-09-13 10:38:36', '2018-09-15 14:18:20'),
(31, 1234567, 'hhh', 'hhh', 'hhhh', 87.98, 'jjjj', 'jjjj', 1, 899, 'kkkkkk', NULL, NULL, '2018-09-15 12:40:09', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_genre`
--

DROP TABLE IF EXISTS `product_genre`;
CREATE TABLE IF NOT EXISTS `product_genre` (
  `product_genre_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `genre_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_genre_id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_genre`
--

INSERT INTO `product_genre` (`product_genre_id`, `product_id`, `genre_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '2018-09-05 16:45:53', NULL),
(2, 1, 3, '2018-09-05 16:45:53', NULL),
(3, 2, 1, '2018-09-05 16:45:53', NULL),
(4, 2, 3, '2018-09-05 16:45:53', NULL),
(5, 3, 4, '2018-09-05 16:45:53', NULL),
(6, 4, 6, '2018-09-05 16:45:53', NULL),
(7, 5, 1, '2018-09-05 16:45:53', NULL),
(8, 6, 1, '2018-09-05 16:45:53', NULL),
(9, 6, 3, '2018-09-05 16:45:53', NULL),
(10, 6, 7, '2018-09-05 16:45:53', NULL),
(11, 7, 9, '2018-09-05 16:45:53', NULL),
(12, 8, 9, '2018-09-05 16:45:53', NULL),
(13, 9, 1, '2018-09-05 16:45:53', NULL),
(14, 9, 3, '2018-09-05 16:45:53', NULL),
(15, 9, 11, '2018-09-05 16:45:53', NULL),
(16, 9, 3, '2018-09-05 16:45:53', NULL),
(17, 9, 11, '2018-09-05 16:45:53', NULL),
(22, 10, 1, '2018-09-05 16:45:53', NULL),
(23, 10, 10, '2018-09-05 16:45:53', NULL),
(24, 11, 12, '2018-09-05 16:45:53', NULL),
(25, 12, 1, '2018-09-05 16:45:53', NULL),
(26, 12, 10, '2018-09-05 16:45:53', NULL),
(27, 13, 1, '2018-09-05 16:45:53', NULL),
(28, 13, 3, '2018-09-05 16:45:53', NULL),
(29, 14, 1, '2018-09-05 16:45:53', NULL),
(30, 14, 10, '2018-09-05 16:45:53', NULL),
(31, 15, 1, '2018-09-05 16:45:53', NULL),
(32, 15, 3, '2018-09-05 16:45:53', NULL),
(33, 15, 7, '2018-09-05 16:45:53', NULL),
(34, 16, 1, '2018-09-05 16:45:53', NULL),
(35, 16, 3, '2018-09-05 16:45:53', NULL),
(36, 17, 1, '2018-09-05 16:45:53', NULL),
(37, 17, 3, '2018-09-05 16:45:53', NULL),
(38, 17, 7, '2018-09-05 16:45:53', NULL),
(39, 18, 1, '2018-09-05 16:45:53', NULL),
(40, 18, 3, '2018-09-05 16:45:53', NULL),
(41, 19, 11, '2018-09-05 16:45:53', NULL),
(42, 20, 6, '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `product_platform`
--

DROP TABLE IF EXISTS `product_platform`;
CREATE TABLE IF NOT EXISTS `product_platform` (
  `product_platform_id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) NOT NULL,
  `platform_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_platform_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_platform`
--

INSERT INTO `product_platform` (`product_platform_id`, `product_id`, `platform_id`, `created_at`, `updated_at`) VALUES
(5, 1, 1, '2018-09-05 16:45:53', NULL),
(6, 1, 2, '2018-09-05 16:45:53', NULL),
(7, 1, 3, '2018-09-05 16:45:53', NULL),
(9, 2, 3, '2018-09-05 16:45:53', NULL),
(10, 2, 4, '2018-09-05 16:45:53', NULL),
(11, 3, 2, '2018-09-05 16:45:53', NULL),
(12, 4, 1, '2018-09-05 16:45:53', NULL),
(13, 4, 2, '2018-09-05 16:45:53', NULL),
(14, 4, 3, '2018-09-05 16:45:53', NULL),
(15, 5, 1, '2018-09-05 16:45:53', NULL),
(16, 5, 2, '2018-09-05 16:45:53', NULL),
(17, 5, 3, '2018-09-05 16:45:53', NULL),
(18, 6, 1, '2018-09-05 16:45:53', NULL),
(19, 6, 2, '2018-09-05 16:45:53', NULL),
(20, 6, 3, '2018-09-05 16:45:53', NULL),
(21, 7, 1, '2018-09-05 16:45:53', NULL),
(22, 7, 2, '2018-09-05 16:45:53', NULL),
(23, 7, 3, '2018-09-05 16:45:53', NULL),
(24, 8, 1, '2018-09-05 16:45:53', NULL),
(25, 8, 2, '2018-09-05 16:45:53', NULL),
(26, 8, 3, '2018-09-05 16:45:53', NULL),
(27, 9, 1, '2018-09-05 16:45:53', NULL),
(28, 9, 2, '2018-09-05 16:45:53', NULL),
(29, 9, 3, '2018-09-05 16:45:53', NULL),
(30, 10, 1, '2018-09-05 16:45:53', NULL),
(31, 10, 2, '2018-09-05 16:45:53', NULL),
(32, 10, 3, '2018-09-05 16:45:53', NULL),
(33, 11, 1, '2018-09-05 16:45:53', NULL),
(34, 11, 2, '2018-09-05 16:45:53', NULL),
(35, 11, 3, '2018-09-05 16:45:53', NULL),
(36, 12, 1, '2018-09-05 16:45:53', NULL),
(37, 12, 2, '2018-09-05 16:45:53', NULL),
(38, 12, 3, '2018-09-05 16:45:53', NULL),
(41, 13, 3, '2018-09-05 16:45:53', NULL),
(42, 12, 1, '2018-09-05 16:45:53', NULL),
(43, 12, 2, '2018-09-05 16:45:53', NULL),
(44, 12, 3, '2018-09-05 16:45:53', NULL),
(47, 13, 3, '2018-09-05 16:45:53', NULL),
(48, 14, 3, '2018-09-05 16:45:53', NULL),
(49, 15, 1, '2018-09-05 16:45:53', NULL),
(50, 15, 2, '2018-09-05 16:45:53', NULL),
(51, 15, 3, '2018-09-05 16:45:53', NULL),
(52, 16, 1, '2018-09-05 16:45:53', NULL),
(53, 16, 2, '2018-09-05 16:45:53', NULL),
(54, 16, 3, '2018-09-05 16:45:53', NULL),
(55, 17, 1, '2018-09-05 16:45:53', NULL),
(56, 17, 2, '2018-09-05 16:45:53', NULL),
(57, 17, 3, '2018-09-05 16:45:53', NULL),
(58, 18, 1, '2018-09-05 16:45:53', NULL),
(59, 18, 2, '2018-09-05 16:45:53', NULL),
(60, 18, 3, '2018-09-05 16:45:53', NULL),
(61, 19, 1, '2018-09-05 16:45:53', NULL),
(62, 19, 2, '2018-09-05 16:45:53', NULL),
(63, 19, 3, '2018-09-05 16:45:53', NULL),
(64, 20, 1, '2018-09-05 16:45:53', NULL),
(65, 20, 2, '2018-09-05 16:45:53', NULL),
(66, 20, 3, '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

DROP TABLE IF EXISTS `rating`;
CREATE TABLE IF NOT EXISTS `rating` (
  `rating_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`rating_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`rating_id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Mature', '2018-09-05 16:45:53', NULL),
(2, '13+', '2018-09-05 16:45:53', NULL),
(3, '17+', '2018-09-05 16:45:53', NULL),
(4, '18+', '2018-09-05 16:45:53', NULL),
(5, 'Rating Pending', '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

DROP TABLE IF EXISTS `supplier`;
CREATE TABLE IF NOT EXISTS `supplier` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `city` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`supplier_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`supplier_id`, `name`, `city`, `created_at`, `updated_at`) VALUES
(1, 'Vip Outlet Winipeg', 'Winnipeg', '2018-09-05 16:45:53', NULL),
(2, 'Jack Stores', 'Calgary', '2018-09-05 16:45:53', NULL),
(3, 'Miller', 'Toronto', '2018-09-05 16:45:53', NULL),
(4, 'Top Games', 'Calgary', '2018-09-05 16:45:53', NULL),
(5, 'Extream Heat', 'Vancouver', '2018-09-05 16:45:53', NULL),
(6, 'Dead Zone', 'Vancouver', '2018-09-05 16:45:53', NULL),
(7, 'Fury X', 'Brampton', '2018-09-05 16:45:53', NULL),
(8, 'Diesel', 'Toronto', '2018-09-05 16:45:53', NULL),
(9, 'Devil Co.', 'Montreal', '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `upcoming_product`
--

DROP TABLE IF EXISTS `upcoming_product`;
CREATE TABLE IF NOT EXISTS `upcoming_product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `publisher` varchar(255) NOT NULL,
  `developer` varchar(200) NOT NULL,
  `price` float NOT NULL,
  `image` varchar(100) NOT NULL,
  `thumbnail` varchar(50) NOT NULL,
  `quantity` int(11) NOT NULL,
  `description` longtext NOT NULL,
  `rating_id` int(11) NOT NULL,
  `supplier` int(11) NOT NULL,
  `platform` varchar(255) NOT NULL,
  `genre` varchar(255) NOT NULL,
  `release_date` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `upcoming_product`
--

INSERT INTO `upcoming_product` (`product_id`, `name`, `publisher`, `developer`, `price`, `image`, `thumbnail`, `quantity`, `description`, `rating_id`, `supplier`, `platform`, `genre`, `release_date`, `created_at`, `updated_at`) VALUES
(1, 'Tomb Raider', 'Square Enix', 'Eidos Montreal', 99.99, 'shadowofthetombraider', 'shadowofthetombraider', 100, 'Shadow of the Tomb Raider is an upcoming action-adventure video game developed by Eidos Montréal in conjunction with Crystal Dynamics and published by Square Enix. It is the sequel to the 2013 game Tomb Raider and its sequel Rise of the Tomb Raider, and the twelfth entry in the Tomb Raider series. The game is set to release worldwide on 14 September 2018 for Microsoft Windows, PlayStation 4 and Xbox One. Set two months after the events of Rise of the Tomb Raider, its story follows Lara Croft as she ventures through Mesoamerica and South America to the legendary city Paititi, battling the paramilitary organization Trinity and racing to stop a Mayan apocalypse she has unleashed. Lara must traverse the environment and combat enemies with firearms and stealth as she explores semi-open hubs. In these hubs she can raid challenge tombs to unlock new rewards, complete side missions, and scavenge for resources which can be used to craft useful materials.', 3, 4, 'PC, PS4, Xbox One', 'Action,Adventure', '14 September, 2018', '2018-09-05 16:45:53', NULL),
(2, 'Hitman 2', 'Warner Bros Interactive Entertainment', 'IO Interactive', 99.99, 'hitman2', 'hitman2', 100, 'Hitman 2 is an upcoming stealth video game developed by IO Interactive and published by Warner Bros. Interactive Entertainment for Microsoft Windows, PlayStation 4, and Xbox One. It will be the seventh entry in the Hitman video game series and is the sequel to the 2016 game Hitman. The game is scheduled to be released on 13 November 2018. After the events of its 2016 predecessor, Hitman, Agent 47 embarks on a mission to hunt the mysterious Shadow Client and disassemble his militia, while also discovering the hidden truth about his past.', 4, 5, 'PC, PS4, Xbox One', 'Stleath', '13 November, 2018', '2018-09-05 16:45:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `postal_code` varchar(255) DEFAULT NULL,
  `province` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `email_id` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT NULL,
  `deleted` tinyint(4) DEFAULT NULL,
  `admin` tinyint(4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email_id` (`email_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `first_name`, `last_name`, `street`, `city`, `postal_code`, `province`, `country`, `telephone`, `email_id`, `user_name`, `password`, `created_at`, `updated_at`, `deleted`, `admin`) VALUES
(10, 'ArshdeepRangi', 'Singh', '1055 Powers Street', 'Winnipeg', 'R2V 2G7', 'Manitoba', 'Canada', '204-881-4887', 'arsh@gmail.com', 'arsh', '$2y$10$GUjScHHP84LjwbQ9ETeLJenqoRd8He9pmSCptRzf43FgvY737GzWS', '2018-09-14 11:20:43', '2018-09-15 09:33:28', 0, 1),
(16, 'Arshdeep', 'Singh', '1055 Powers Street', 'Winnipeg', 'R2V 2G7', 'Manitoba', 'Canada', '204-881-4887', 'arsh1@gmail.com', 'arsh1', '$2y$10$lQ6lJhDtObtwB4233abeLum/RBJ08x1pCYRnt6rI0O6yGWhgf7TQC', '2018-09-14 16:45:45', '2018-09-14 16:49:05', 0, 0),
(17, 'Navdeep', 'Dhindsa', '1150 Munroe Ave', 'Winnipeg', 'R2V 2G7', 'Manitoba', 'Canada', '204-881-4887', 'nav@gmail.com', 'nav', '$2y$10$i.xr3DfN/aqMkT3GCPn1/uqNzxkbmkUFPJwYsSUYC27.D/50wWPby', '2018-09-15 12:15:22', NULL, 0, 0);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
