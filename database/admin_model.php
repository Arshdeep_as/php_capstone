<?php
/**File name: admin_model.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: file to store the functions related to admin searches.
*/

/**
 * function to return all the tables present in the gaming_db.
 * @param  [object] $dbh passing the database object.
 * @return [array] list of tables present in the databse
 */
function getAllRelations($dbh)
{

    $query = "show tables";
    $stmt = $dbh->prepare($query);
    //$stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to fetch the aggregate values from the product table
 * @param  [object] $dbh passing the database object.
 * @return [array]      
 */
function getAggregateValues($dbh)
{
    $query ='SELECT
            count(product.product_id) as product_count,
            round(max(product.price),2) as max_price,
            round(min(product.price),2) as min_price,
            round(avg(product.price),2) as avg_price 
            FROM product';
    $stmt = $dbh->prepare($query);
    //$stmt->bindValue(':tablename', $tablename, PDO::PARAM_STR);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to geet the deleted products count
 * @param  [object] $dbh passing the database object.
 * @return [number]   
 */
function getDeletedCount($dbh)
{
    $query ='SELECT
            count(deleted_product.id) as delete_count
            FROM deleted_product';
    $stmt = $dbh->prepare($query);
    //$stmt->bindValue(':tablename', $tablename, PDO::PARAM_STR);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}
/**
 * function to select records from one table
 * @param  [object] $dbh passing the database object.
 * @param  [string] $tablename name of the table
 * @return [array] list of product details.
 */
function getAlldetails($dbh, $tablename)
{

	$query ='SELECT product.product_id, product.name, product.publisher, product.developer, product.price, product.image, product.quantity, product.in_stock, rating.name as rating, supplier.name as supplier FROM product JOIN rating USING(rating_id) JOIN supplier USING(supplier_id) ORDER BY product_id ASC';
    $stmt = $dbh->prepare($query);
    //$stmt->bindValue(':tablename', $tablename, PDO::PARAM_STR);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to fetch the details of the product as per product_id 
 * @param  [object] $dbh database object
 * @param  [int] $product_id product_id of the product
 * @return [array]  
 */
function getProductDetails($dbh, $product_id)
{
    $query ='SELECT product.product_id, product.name, product.publisher, product.developer, product.price, product.image, product.thumbnail, product.quantity, product.in_stock,product.rating_id as rating_id, product.supplier_id as supplier_id, rating.name as rating, supplier.name as supplier FROM product JOIN rating USING(rating_id) JOIN supplier USING(supplier_id) WHERE product_id = :product_id';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_INT);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to fetch the rating name as per rating_id.
 * @param  [object] $dbh the database object
 * @param  [int] $rating_id rating id whose name is wanted
 * @return [array] 
 */
function getRatingName($dbh, $rating_id)
{
    $query ='SELECT rating_id, name FROM rating WHERE rating_id = :rating_id';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':rating_id', $rating_id, PDO::PARAM_INT);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to fetch the supplier name as per supplier_id.
 * @param  [object] $dbh the database object
 * @param  [int] $supplier_id supplier id whose name is wanted
 * @return [array] 
 */
function getSupplierName($dbh, $supplier_id)
{
    $query ='SELECT supplier_id, name FROM supplier WHERE supplier_id = :supplier_id';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':supplier_id', $supplier_id, PDO::PARAM_INT);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}


/**
 * function to get details of the product which is going to be deleted
 * @param  [object] $dbh database object, handler
 * @param  [int] $product_id id of the product which is to deleted
 * @return array
 */
function getProductRecord($dbh, $product_id)
{
    $query ='SELECT * FROM product WHERE product_id = :product_id';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_INT);
    //$param = array(':tablename' => $tablename);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
}

/**
 * function to insert the product in delete archive table before deleting
 * @param  [object] $dbh database object
 * @param  [array] $deleted details of the product which is going to be deleted
 * @return boolean 
 */
function insertDeletedProduct($dbh, $deleted)
{
    $query = "INSERT INTO deleted_product 
                ( 
                    product_id,
                    product_sku,
                    name,
                    publisher,
                    developer,
                    price,
                    image,
                    thumbnail,
                    in_stock,
                    quantity,
                    description,
                    rating_id,
                    supplier_id                 
                )
                values
                (
                    :product_id,
                    :product_sku,
                    :name,
                    :publisher,
                    :developer,
                    :price,
                    :image,
                    :thumbnail,
                    :in_stock,
                    :quantity,
                    :description,
                    :rating_id,
                    :supplier_id
                )";

        $stmt = $dbh->prepare($query);

        $params = array
                (
                    ':product_id' =>$deleted['product_id'],
                    ':product_sku'=>$deleted['product_sku'],
                    ':name'=>$deleted['name'],
                    ':publisher'=>$deleted['publisher'],
                    ':developer'=>$deleted['developer'],
                    ':price'=>$deleted['price'],
                    ':image'=>$deleted['image'],
                    ':thumbnail'=>$deleted['thumbnail'],
                    ':in_stock'=>$deleted['in_stock'],
                    ':quantity'=>$deleted['quantity'],
                    ':description'=>$deleted['description'],
                    ':rating_id'=>$deleted['rating_id'],
                    ':supplier_id'=>$deleted['supplier_id']
                );

        if($stmt -> execute($params)){
            return true;
        }
        else{
            return false;
        }
}

/**
 * function to delete the product from the product table.
 * @param  [object] $dbh database object, handler
 * @param  [int] $product_id id of the product which is to deleted
 * @return [boolean] 
 */
function deleteProduct($dbh, $product_id)
{
    $query ='DELETE FROM product WHERE product_id = :product_id';
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_INT);
    
    if($stmt -> execute()){
        return true;
    }
    else{
        return false;
    }
    
}

/**
 * function for admin searches.
 * @param  [object] $dbh  database object, handler
 * @param  [string] $term the term which admin searches
 * @return [array] 
 * */
function searchProduct($dbh, $term)
{
    $query = "SELECT product.product_id, product.name, product.publisher, product.developer, product.price, product.image, product.quantity, product.in_stock, rating.name as rating, supplier.name as supplier FROM product JOIN rating USING(rating_id) JOIN supplier USING(supplier_id) WHERE product.name LIKE CONCAT('%', :term, '%') ORDER BY product_id ASC";
    $stmt = $dbh->prepare($query);
    $stmt -> bindValue(':term', $term, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $results; 
}

/**
 * function to insert the new product into the product table.
 * @param  [object] $dbh database object handler
 * @param  [array] $product details of the product that is to be inserted
 * @return boolean
 */
function insertIntoProduct($dbh, $product)
{
    $query = "INSERT INTO product 
                ( 
                    product_sku,
                    name,
                    publisher,
                    developer,
                    price,
                    image,
                    thumbnail,
                    in_stock,
                    quantity,
                    description,
                    rating_id,
                    supplier_id                 
                )
                values
                (
                    :product_sku,
                    :name,
                    :publisher,
                    :developer,
                    :price,
                    :image,
                    :thumbnail,
                    :in_stock,
                    :quantity,
                    :description,
                    :rating_id,
                    :supplier_id
                )";

        $stmt = $dbh->prepare($query);

        $params = array
                (
                    ':product_sku'=>$product['product_sku'],
                    ':name'=>$product['name'],
                    ':publisher'=>$product['publisher'],
                    ':developer'=>$product['developer'],
                    ':price'=>$product['price'],
                    ':image'=>$product['image'],
                    ':thumbnail'=>$product['thumbnail'],
                    ':in_stock'=>$product['in_stock'],
                    ':quantity'=>$product['quantity'],
                    ':description'=>$product['description'],
                    ':rating_id'=>$product['rating'],
                    ':supplier_id'=>$product['supplier']
                );
        return $stmt -> execute($params);
}

/**
 * function to update a product in the database
 * @param  [object] $dbh database object handler
 * @param  [array] $product details of the product that is to be updated
 * @return boolean
 */
function updateProduct($dbh, $product)
{
    $query = "UPDATE product SET 
                    name = :name,
                    publisher = :publisher,
                    developer = :developer,
                    price = :price,
                    image = :image,
                    thumbnail = :thumbnail,
                    in_stock = :in_stock,
                    quantity = :quantity,
                    rating_id = :rating_id,
                    supplier_id = :supplier_id,
                    updated_at = current_timestamp()
                    WHERE product_id = :product_id";

        $stmt = $dbh->prepare($query);

        $params = array
                (
                    ':name'=>$product['name'],
                    ':publisher'=>$product['publisher'],
                    ':developer'=>$product['developer'],
                    ':price'=>$product['price'],
                    ':image'=>$product['image'],
                    ':thumbnail'=>$product['thumbnail'],
                    ':in_stock'=>$product['in_stock'],
                    ':quantity'=>$product['quantity'],
                    ':rating_id'=>$product['rating_id'],
                    ':supplier_id'=>$product['supplier_id'],
                    ':product_id'=>$product['product_id'],
                );

        return $stmt -> execute($params);
}