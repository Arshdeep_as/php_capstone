<?php

/**
 * function to fetch the entered details of user.
 * @param  [object] $dbh, database object
 * @param  [int] $user_id is the id of the user whose details you want.
 * @return array
 */
function fetchUserDetails($dbh, $user_id)
{
    $query = "SELECT user_id, first_name, last_name, street, city, postal_code, province, country, telephone, email_id, user_name from user WHERE user_id = :user_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    return $user;
}

/**
 * function to update the user data in the database.
 * @param  [object] $dbh, database object
 * @param  [array] $entered_details is array of deatils user submitted
 * @return boolean
 */
function updateUserProfile($dbh, $entered_details)
{
	$query = "UPDATE user SET 
            first_name = :first_name,
            last_name = :last_name,
            street = :street,
            city = :city,
            postal_code = :postal_code,
            province = :province,
            country = :country,
            telephone = :telephone,
            email_id = :email_id,
            user_name = :user_name,
            updated_at = current_timestamp()
            WHERE user_id = :user_id";

  $stmt = $dbh->prepare($query);

        $params = array
                (
                ':first_name'=>$entered_details['first_name'],
                ':last_name'=>$entered_details['last_name'],
                ':street'=>$entered_details['street'],
                ':city'=>$entered_details['city'],
                ':postal_code'=>$entered_details['postal_code'],
                ':province'=>$entered_details['province'],
                ':country'=>$entered_details['country'],
                ':user_name'=>$entered_details['user_name'],
                ':email_id'=>$entered_details['email_id'],
                ':telephone'=>$entered_details['telephone'],
                ':user_id'=>$entered_details['user_id']
                );
	return $stmt -> execute($params);
}

/**
 * function to fetch the old password from the database.
 * @param  [object] $dbh, database object
 * @param  [int] $user_id is the id of the user whose password you want.
 * @return array
 */
function getOldPassword($dbh, $user_id)
{
	$query = "SELECT password from user WHERE user_id = :user_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':user_id', $user_id, PDO::PARAM_INT);
    $stmt->execute();
    $oldpassword = $stmt->fetch(PDO::FETCH_ASSOC);
    return $oldpassword;
}

/**
 * function to update the user password
 * @param  [object] $dbh, database object
 * @param  [int] $user_id is the id of the user whose password you want to change.
 * @param  [string] $password that is entered by the user
 * @return boolean
 */
function updatePassword($dbh, $user_id, $password)
{
	$query = "UPDATE user SET 
            password = :password,
            updated_at = current_timestamp()
            WHERE user_id = :user_id";

  $stmt = $dbh->prepare($query);

        $params = array
                (
                ':password'=>password_hash($password, PASSWORD_DEFAULT),
                ':user_id'=>$user_id
                );
	return $stmt -> execute($params);
}

/**
 * function to insert data in the incoice.
 * @param  [object] $dbh, database object
 * @param  [array] $data is an array of deatils needed to br entered in the invoice table.
 * @return boolean
 */
function insertInInvoice($dbh, $data){
    $query = "INSERT INTO invoice
            (
                user_id,
                invoice_date,
                sub_total,
                gst,
                pst,
                total
            ) VALUES
            (
                :user_id,
                current_timestamp(),
                :sub_total,
                :gst,
                :pst,
                :total
            )";

        $stmt = $dbh->prepare($query);

        $params = array
                (
                ':user_id'=>$data['user_id'],
                ':sub_total'=>$data['subtotal'],
                ':gst'=>$data['gst'],
                ':pst'=>$data['pst'],
                ':total'=>$data['total']
                );
        return $stmt->execute($params);
}

/**
 * get details from invoice table
 * @param  [object] $dbh, database object
 * @param  [int] $invoice_id as name describes
 * @return array
 */
function getInvoiceDetails($dbh, $invoice_id)
{
    $query = "SELECT * from invoice WHERE invoice_id = :invoice_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':invoice_id', $invoice_id, PDO::PARAM_INT);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
    return $result;
}


/**
 * function to insert data in the incoiceproduct.
 * @param  [object] $dbh, database object
 * @param  [String] $inserted_id is the id inserted in the invoice table.
 * @param  [Array] $product_ids The product_ids to be inserted
 * @return boolean
 */
function insertInvoiceProduct($dbh,$inserted_id,$product_ids){

    for($i=0;$i<count($product_ids);$i++){
    $query = "INSERT INTO invoice_product
            (
                invoice_id,
                product_id
            ) VALUES
            (
                :invoice_id,
                :row
            )";

        $stmt = $dbh->prepare($query);

        $params = array
                (
                ':invoice_id'=>$inserted_id,
                ':row'=>$product_ids[$i]
                );
        $stmt->execute($params);
    }
    return true;
}
