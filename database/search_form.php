<?php
/**File name: search_form.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file for ajax search
*/
if($_SERVER['REQUEST_METHOD'] == 'POST'){
  //print_r($_POST);
    $term = $_POST['search'];
    //print_r($term);
    //connect to database
    if(!empty($term)){

      $dbh = new PDO('mysql:host=localhost;dbname=gaming_db', 'root', '');
      $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

  		$query = "SELECT product_id, name FROM product WHERE name LIKE CONCAT(:term, '%')";
  		$stmt = $dbh -> prepare($query);
      $stmt -> bindValue(':term', $term, PDO::PARAM_STR);
  		$stmt -> execute();

  		$book = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  		//var_dump($book);
      //var_dump($term);
      header('Content-type: application/json');
      echo json_encode($book);
    }

 }