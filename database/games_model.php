<?php
/**File name: games_model.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file to store the functions related to the games searches.
*/
  
  /**
   * function to fetch the game titles as per oplatform defined
   * @param  [object] $dbh passing the database object.
   * @param  [string] $platform platform name corressponding to which games needed to be fetched.
   * @return [array] returns the array of games titles fetched
   */
  function fetchGames($dbh, $platform)
  {
    $query = "SELECT product.product_id, product.name, product.image, product.price, platform.platform_name from product left join product_platform on product.product_id = product_platform.product_id left join platform on product_platform.platform_id = platform.platform_id WHERE platform.platform_name = :platform";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  /** 
   * funtion to fetch the random games as per platform defined.
   * @param  [object] $dbh passing the database object.
   * @param  [string] $platform platform name corressponding to which games needed to be fetched.
   * @return [array] returns the array of games titles fetched
   */
  function fetchRandomGames($dbh, $platform)
  {
    $query = "SELECT product.product_id, product.name, product.image, product.price, platform.platform_name as platform FROM product LEFT JOIN product_platform ON product.product_id = product_platform.product_id LEFT JOIN platform ON product_platform.platform_id = platform.platform_id WHERE platform.platform_name = :platform";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $random_keys=array_rand($results, 2);
    $random = array();
    
    foreach($random_keys as $value){
      $random[$value]=$results[$value];
    }
    return $random;
  }

  /**
   * function to fetch the product details as per product_id specified
   * @param  [object]  $dbh passing the database object.
   * @param  [integer] $product_id passing the product_id
   * @return [array] returns the array of games titles fetched
   */
  function fetchProductDetails($dbh, $product_id){
    $query = "SELECT product.product_id, product.name, product.publisher, product.developer, product.price, product.image, product.thumbnail, product.description, rating.name as rating FROM product JOIN rating USING(rating_id) WHERE product_id = :product_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * function to fetch product details for session
   * @param  [object] $dbh  passing the database object.
   * @param  [integer] $product_id passing the product_id
   * @return [array] an array of results  found
   */
 function fetchProductDetails2($dbh, $product_id){
    $query = "SELECT product.product_id, product.name FROM product WHERE product_id = :product_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * function to fetch product details for cart
   * @param  [object] $dbh  passing the database object.
   * @param  [integer] $product_id passing the product_id
   * @return [array] an array of results  found
   */
  function fetchProductDetails3($dbh, $product_id){
    $query = "SELECT product.product_id, product.name,  product.price, product.image  FROM product  WHERE product_id = :product_id";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetch(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * fetching the products with similar platform
   * @param  [object] $dbh  passing the database object.
   * @param  [integer] $product_id passing the product_id
   * @return [array] an array of results  found
   */
  function fetchProductPlatform($dbh, $product_id){
    $query = "SELECT platform.platform_name FROM product LEFT JOIN product_platform ON product.product_id = product_platform.product_id LEFT JOIN platform ON product_platform.platform_id = platform.platform_id WHERE product.product_id = :product_id;";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * fetch the products by genre from the database
   * @param  [object] $dbh  passing the database object.
   * @param  [integer] $product_id passing the product_id
   * @return [array] an array of results  found
   */
  function fetchProductGenre($dbh, $product_id){
    $query = "SELECT genre.name FROM product left join product_genre on product.product_id = product_genre.product_id  left join genre on product_genre.genre_id = genre.genre_id WHERE product.product_id = :product_id;";
    $stmt = $dbh->prepare($query);
    $stmt->bindValue(':product_id', $product_id, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    return $results;
  }

  /**
   * fetching the upcoming games from the database table
   * @param  [object] $dbh  passing the database object.
   * @return [array] an array of results  found
   */
  function fetchUpcomingGames($dbh){
    $query = "SELECT upcoming_product.product_id, upcoming_product.name, upcoming_product.image, upcoming_product.price, upcoming_product.platform FROM upcoming_product";
    $stmt = $dbh->prepare($query);
    //$stmt->bindValue(':platform', $platform, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $random_keys=array_rand($results, 2);
    $random = array();
    
    foreach($random_keys as $value){
      $random[$value]=$results[$value];
    }
    return $random;
  }

  /**
   * Searching games with the title
   * @param  [object] $dbh  passing the database object.
   * @param  [string] $search the keyword to be searched
   * @return [array] an array of results  found
   */
  function searchGames($dbh, $search){
    $query = "SELECT * FROM product WHERE name = :search";
    $stmt = $dbh->prepare($query);
    $stmt -> bindValue(':search', $search, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $results; 
  }

  /**
   * Searching the product with the title name
   * @param  [object]  $dbh passing the database object.
   * @param  [string] $title the titlle to be searched
   * @return [array] an array of results  found
   */
  function searchedProductPlatform($dbh, $title){
    $query = "SELECT platform.platform_name FROM product left join product_platform on product.product_id = product_platform.product_id
              left join platform on product_platform.platform_id = platform.platform_id
              WHERE product.name = :title";
    $stmt = $dbh->prepare($query);
    $stmt -> bindValue(':title', $title, PDO::PARAM_STR);
    $stmt -> execute();
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);

    return $results; 

  }

  