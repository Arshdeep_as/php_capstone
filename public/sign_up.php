<?php
/**
 *Description : allow user to sign up to the website.
 *@filename sign_up.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */
  $title = "Sign Up";
  $heading = "Create your Account";
  require __DIR__.'/../config.php';
  //validator class, storing all the validation functions.
  use \Classes\Utility\Validator;
  //making a new object of validator class
  $validate = new Validator();
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require 'validate.php';

  //array to hold the values of errors.
  $errors = [];
  //initializing the success flag
  $success = false;
  
  //if form method is post then only we are going to do or validations and everything.
  if($_SERVER['REQUEST_METHOD']=="POST"){
    //checking if the predefined csrf matches the csrf provided by the post request. 
    if($_POST['site_csrf'] == $_SESSION['site_csrf']){

      //validating the form fields to check if user entered the correct information
      $required_form_fields = ['first_name','last_name','street','city','postal_code','province','country','telephone','user_name','password'];
      $only_character_fields = ['first_name','last_name','city','province','country'];
      $validate->required($required_form_fields);
      $validate->onlyCharacters($only_character_fields);
      $validate->emailValidation('email_id');
      $validate->telephoneValidation('telephone');
      $validate->postalCodeValidation('postal_code');
      $validate->streetNameValidation('street');
      $validate->passwordValidation('password', 'confirm_password');
      //$validate->checkForUsername('email_id');
      $errors = $validate->errors();
      
      //if there are no errors in the data provided by the user, then we are going to insert that data into the database.
      if(count($validate->errors()) == 0) {
        //for deleted field in user table.
        $deleted = 0;
    
        try {
      
          //connection to database.
          //connect to mysql using database handler.
          $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
          //setting database handler to show errors.
          $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
          //create query to insert records into our database.
          $query = "INSERT INTO user(first_name, last_name, street, city, postal_code, province, country, telephone, email_id, user_name, password, deleted)
   VALUES (:first_name, :last_name, :street, :city, :postal_code, :province, :country, :telephone, :email_id, :user_name, :password, :deleted)";
      
          //prepare query
          $stmt = $dbh->prepare($query);
      
          //bind values to the variable we used in the query.
          $stmt->bindValue(':first_name', $_POST['first_name'], PDO::PARAM_STR);
          $stmt->bindValue(':last_name', $_POST['last_name'], PDO::PARAM_STR);
          $stmt->bindValue(':street', $_POST['street'], PDO::PARAM_STR);
          $stmt->bindValue(':city', $_POST['city'], PDO::PARAM_STR);
          $stmt->bindValue(':postal_code', $_POST['postal_code'], PDO::PARAM_STR);
          $stmt->bindValue(':province', $_POST['province'], PDO::PARAM_STR);
          $stmt->bindValue(':country', $_POST['country'], PDO::PARAM_STR);
          $stmt->bindValue(':telephone', $_POST['telephone'], PDO::PARAM_STR);
          $stmt->bindValue(':email_id', $_POST['email_id'], PDO::PARAM_STR);
          $stmt->bindValue(':user_name', $_POST['user_name'], PDO::PARAM_STR);
          $stmt->bindValue(':password', password_hash($_POST['password'], PASSWORD_DEFAULT), PDO::PARAM_STR);
          $stmt->bindValue(':deleted', $deleted, PDO::PARAM_INT);
          //execute query
          if ($stmt->execute()) {
            //if this query executes, we are going to fetch the id of the last record we have just submitted.
            $id = $dbh->lastInsertId();
        
        
            //create query to select the required information from the record
            $query = "SELECT user_name from user WHERE user_id = :id";
        
            //prepare query
            $stmt = $dbh->prepare($query);
        
            //bind value to the variables used in the query.
            $stmt->bindValue(':id', $id, PDO::PARAM_INT);
        
            //execute query
            $stmt->execute();
        
            //fetch results of the query in this variable.
            $lastuser = $stmt->fetch(PDO::FETCH_ASSOC);
        
            //set boolean success flag.
            $success = true;
            $_SESSION['user_id'] = $id;
            $_SESSION['user_name'] = $lastuser['user_name'];
            $_SESSION['msg'] = 'Congratulations for login!';
            $_SESSION['logged_in'] = true;
            session_regenerate_id();
        
            header('Location: profile.php');
            die;
        
          } else {
            //if the execute query for insert data doesn't work, this means data is not inserted in the database.
            die('There was problem in inserting the record');
          }//end if statement for execute query(insert statement).
      
        }//end of try statement
    
        catch (PDOException $e) {
          print($e->getMessage());
          $sqlerror= $e->getMessage();
          if(!empty($sqlerror)){
            if(strpos($sqlerror,'email_id')){
            $errors['email_id']="Sorry, this email is already taken!";
            }
            elseif(strpos($sqlerror,'user_name')){
              $errors['user_name']="Sorry, this user name is already taken!";
            }
          }        
        }//end of catch statement
      }//end if statement for empty errors(need to connect to DB).    
    }//end if site csrf matches form csrf.
    else{
      die('Sorry, Nice try but you failed.');
    }  

  }// end if statement for server method check.
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        
        <?php if($success == false) :?>
				<div id="registrationForm">
					<h1><?=$heading;?></h1>
     
					<form method="post"
								action="sign_up.php"
								id="contact_info"
								name="contact_info" 
								autocomplete="on"
                accept-charset="UTF-8"
                novalidate="novalidate">
						<fieldset><!--Opens required information fieldset-->
							<legend>Required Information</legend>
              <!-- Setting up the csrf topken in the hidden field-->
              <input type="hidden" name="site_csrf" value="<?=esc_attr($_SESSION['site_csrf']);?>" />
							<p><!--Text Fields and Password Fields-->
                <label class="labelWidth" for="first_name">First Name<span class="required_field"> *</span></label>
                <input type="text" name="first_name" required maxlength="25" size="25" value="<?php if(!empty($_POST['first_name'])){ echo esc_attr($_POST['first_name']); } ?>"/>
							  <br />
                <?php if(!empty($errors['first_name'])) :?>
                  <span class="show_error"><small><?=esc($errors['first_name']);?></small></span>
                <?php endif; ?>
              </p>
							<p>
								<label class="labelWidth" for="last_name">Last Name<span class="required_field"> *</span></label>
                <input type="text" name="last_name" required maxlength="25" size="25" value="<?php if(!empty($_POST['last_name'])){ echo esc_attr($_POST['last_name']); } ?>"/>
                <br />
                <?php if(!empty($errors['last_name'])) :?>
                  <span class="show_error"><small><?=esc($errors['last_name']);?></small></span>
                <?php endif; ?>
              </p>
              <p>
                <label class="labelWidth" for="street">Street<span class="required_field"> *</span></label>
                <input type="text" id="street" name="street" required maxlength="25" size="25" value="<?php if(!empty($_POST['street'])){ echo esc_attr($_POST['street']); } ?>" />
                <br />
                <?php if(!empty($errors['street'])) :?>
                  <span class="show_error"><small><?=esc($errors['street']);?></small></span>
                <?php endif; ?>
              </p>
              <p>
                <label class="labelWidth" for="city">City<span class="required_field"> *</span></label>
                <input type="text" id="city" name="city" required maxlength="25" size="25" value="<?php if(!empty($_POST['city'])){ echo esc_attr($_POST['city']); } ?>" />
                <br />
                <?php if(!empty($errors['city'])) :?>
                  <span class="show_error"><small><?=esc($errors['city']);?></small></span>
                <?php endif; ?>
              </p>
              
              <p>
                <label class="labelWidth" for="postal_code">Postal Code<span class="required_field"> *</span></label>
                <input type="text" id="postal_code" name="postal_code" required maxlength="25" size="25" value="<?php if(!empty($_POST['postal_code'])){ echo esc_attr($_POST['postal_code']); } ?>" />
                <br />
                <?php if(!empty($errors['postal_code'])) :?>
                  <span class="show_error"><small><?=esc($errors['postal_code']);?></small></span>
                <?php endif; ?>
              </p>
              
              <p>
                <label class="labelWidth" for="province">Province<span class="required_field"> *</span></label>
                <input type="text" id="province" name="province" required maxlength="25" size="25" value="<?php if(!empty($_POST['province'])){ echo esc_attr($_POST['province']); } ?>" />
                <br />
                <?php if(!empty($errors['province'])) :?>
                  <span class="show_error"><small><?=esc($errors['province']);?></small></span>
                <?php endif; ?>
              </p>
              <p>
                <label class="labelWidth" for="country">Country<span class="required_field"> *</span></label>
                <input type="text" id="country" name="country" required maxlength="25" size="25" value="<?php if(!empty($_POST['country'])){ echo esc_attr($_POST['country']); } ?>" />
                <br />
                <?php if(!empty($errors['country'])) :?>
                  <span class="show_error"><small><?=esc($errors['country']);?></small></span>
                <?php endif; ?>
              </p>
              
              <p>
                <label class="labelWidth" for="telephone">Contact No.<span class="required_field"> *</span></label>
                <input type="tel" id="telephone" name="telephone" required maxlength="25" size="25" value="<?php if(!empty($_POST['telephone'])){ echo esc_attr($_POST['telephone']); } ?>" />
                <br />
                <?php if(!empty($errors['telephone'])) :?>
                  <span class="show_error"><small><?=esc($errors['telephone']);?></small></span>
                <?php endif; ?>
              </p>
              
							<p>
								<label class="labelWidth" for="email_id">Email ID<span class="required_field"> *</span></label>
								<input type="email" id="email_id" name="email_id" required maxlength="25" size="25" value="<?php if(!empty($_POST['email_id'])){ echo esc_attr($_POST['email_id']); } ?>" />
                <br />
                <?php if(!empty($errors['email_id'])) :?>
                  <span class="show_error"><small><?=esc($errors['email_id']);?></small></span>
                <?php endif; ?>
							</p>
       
							<p>
								<label class="labelWidth" for="user_name">User Name<span class="required_field"> *</span></label>
								<input type="text" id="user_name" name="user_name" required maxlength="25" size="25" value="<?php if(!empty($_POST['user_name'])){ echo esc_attr($_POST['user_name']); } ?>" />
                <br />
                <?php if(!empty($errors['user_name'])) :?>
                  <span class="show_error"><small><?=esc($errors['user_name']);?></small></span>
                <?php endif; ?>
							</p>
							<p>
								<label class="labelWidth" for="password">Password<span class="required_field"> *</span></label>
								<input type="password" id="password" name="password" required maxlength="25" size="25"  />
							</p>
							<p>
								<label class="labelWidth" for="confirm_password">Confirm Password<span class="required_field">*</span></label>
								<input type="password" id="confirm_password" name="confirm_password" required maxlength="25" size="25" />
                <br />
                <?php if(!empty($errors['password'])) :?>
                  <span class="show_error"><small><?=esc($errors['password']);?></small></span>
                <?php endif; ?>
							</p>
						</fieldset>
						
						<!--Buttons at the end of the form-->
						<p id="formButton">
							<input type="submit" class="sign_up_formbutton" value="Create Account" id="send_form"> &nbsp;&nbsp;
							<input type="reset" class="sign_up_formbutton" value="Reset Form" id="clear_form"> &nbsp;&nbsp;
						</p>
						
						<p>
							You will recieve an email notification for confirming your email ID.
						</p>
						<p>
							For any feedback back and additional comments please contact us on our contact us page.
						</p>
					</form>
				</div>
        <?php endif;?>
        
        <?php if($success == true) :?>
          <div id="results">
            <h1 style="text-align: center">Thank you for registering!</h1>
            <h2 style="text-align: center">You submitted the following information:</h2>
            <hr />
            <table>
              <tr>
                <th>Asked</th>
                <th>Information Provided</th>
              </tr>
              <?php foreach( $lastuser as $key => $value) : ?>
                <tr>
                  <td><?=ucwords(str_replace('_', ' ', $key));?></td>
                  <td><strong><?=$value;?></strong></td>
                </tr>
              <?php endforeach; ?>
            </table>
            <div class="button"><a href="index.php"><span>Home</span></a></div>
          </div>
          
        <?php endif; ?>
				
			</div>
			
		</div>
  
<?php
  include '../includes/footer.inc.php';
?>