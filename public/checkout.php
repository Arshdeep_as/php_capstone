<?php
/**File name: checkout.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file for checkout page to insert into invoice table
*/
  $title = "Checkout";
  require __DIR__.'/../config.php';
  
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  include '../database/games_model.php';
  require 'validate.php';
  
  define('GST', '0.05');
  define('PST', '0.08');

  $details=$_SESSION['cart'];
  if(empty($_SESSION['cart'])){
    $_SESSION['empty_cart']=true;
    $_SESSION['empty_cart_message']="Your cart is empty, please select some items!";
    header('Location: pc_games.php');
    die;
  }

  if(!empty($_SESSION['cart'])){
    
   // $product_id = $_SESSION['cart'];
     
  //var_dump($product_id);
  //$details=[];
  $sub_total = 0;
  
  foreach ($details as $row) {
    $sub_total += $row['price'];
  }

  $after_pst = round($sub_total * PST, 2);
  $after_gst = round($sub_total * GST, 2);

  $grand_total = $sub_total + $after_pst + $after_gst;

  $_SESSION['prices']=[
    'gst'=>$after_gst,
    'pst'=>$after_pst,
    'total'=>$grand_total,
    'subtotal'=>$sub_total

  ];

  $last_id = '';
  $last_platform = '';

  //var_dump($details);

  }
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				<h1>Checkout page</h1>
        <table>
          <tr>
            <th>Name</th>
            <th>Platform</th>
            <th>Price</th>
          </tr>

          <?php foreach($details as $row) :?>
            <tr>
              <td><?=esc_attr($row['name'])?></td>
              <td><?=esc_attr($row['platform'])?></td>
              <td>$<?=esc_attr($row['price'])?></td>
              <?php 
                $last_id = esc_attr($row['product_id']);
                $last_platform = esc_attr($row['platform']);
              ?>
            </tr>
          <?php endforeach; ?>

          <tr>
            <td colspan="2">Sub Total</td>
            <td><strong>$<?=esc_attr($sub_total);?></strong></td>
          </tr>

          <tr>
            <td colspan="2">PST</td>
            <td>$<?=esc_attr(round($after_pst,2));?></td>
          </tr>

          <tr>
            <td colspan="2">GST</td>
            <td>$<?=esc_attr(round($after_gst,2))?></td>
          </tr>

          <tr>
            <td colspan="2">Total</td>
            <td><strong>$<?=esc_attr(round($grand_total,2));?></strong></td>
          </tr>

          
        </table>
        
        <p id="checkout_page">
          <a href="product_details.php?product_id=<?=$last_id?>&platform=<?=$last_platform?>">Continue Shopping</a>
          <a href="payment.php">Complete Purchase</a>
        </p>
			</div>
			
		</div>

<?php
  include '../includes/footer.inc.php';
?>