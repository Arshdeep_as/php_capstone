<?php
/**File name: checkout.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file for checkout page to insert into invoice table
*/
  $title = "Checkout";
  require __DIR__.'/../config.php';
  
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  include '../database/games_model.php';
  include '../database/user_model.php';
  //validator class, storing all the validation functions.
  use \Classes\Utility\Validator;
  $validate = new Classes\Utility\Validator();
  require 'validate.php';
  
  if(!isset($_SESSION['user_id'])){
    die('You must login to proceed');
  }

  $id = $_SESSION['user_id'];
  if(!empty($_SESSION['cart'])){
    $details=$_SESSION['cart'];
     
    //var_dump($product_id);
    
    $sub_total = 0;
    $product_ids=[];
    
      foreach($_SESSION['cart'] AS $row){
        
        array_push($product_ids,$row['product_id']);
      }


  }

  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //checking if the predefined csrf matches the csrf provided by the post request.
    if($_POST['site_csrf'] == $_SESSION['site_csrf']){

      $required_form_fields = ['name','card_number','exp_data','cvv'];
      $validate->required($required_form_fields);
      $errors = $validate->errors();
      if(count($validate->errors()) == 0){

        $invoice_data = [
          'user_id' => $id,
          'subtotal' => $_SESSION['prices']['subtotal'] ,
          'gst' => $_SESSION['prices']['gst'],
          'pst' => $_SESSION['prices']['pst'],
          'total'=> $_SESSION['prices']['total']
        ];
        //var_dump($invoice_data);
        $isinserted = insertInInvoice($dbh, $invoice_data);
        if($isinserted){
        $inserted_id = $dbh->lastInsertId();
        }
        $isinsertedproduct = insertInvoiceProduct($dbh,$inserted_id,$product_ids);
        if($isinsertedproduct){
          
          $_SESSION['invoice_id'] = $inserted_id;
          header('Location:thankyou.php');
          die;
        }
      }
    }//end csrf token match
  }//end if if server request method.
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				<h1>Checkout page</h1>
        <table>
          <tr>
            
            <th>Name</th>
            <th>Platform</th>
            <th>Price</th>
          </tr>

          <?php foreach($details as $row) :?>
            <tr>
              
              <td><?=$row['name']?></td>
              <td><?=$row['platform']?></td>
              <td>$<?=$row['price']?></td>
            </tr>
          <?php endforeach; ?>

          <tr>
            <td colspan="2">Sub Total</td>
            <td><strong>$<?=$_SESSION['prices']['subtotal'];?></strong></td>
          </tr>

          <tr>
            <td colspan="2">PST</td>
            <td>$<?=$_SESSION['prices']['pst'];?></td>
          </tr>

          <tr>
            <td colspan="2">GST</td>
            <td>$<?=$_SESSION['prices']['gst'];?></td>
          </tr>

          <tr>
            <td colspan="2">Total</td>
            <td><strong>$<?=$_SESSION['prices']['total'];;?></strong></td>
          </tr>

          
        </table>

        <h1>Enter your card Details</h1>
        <form class="my_form" action="payment.php" method="post" autocomplete="off" novalidate>
          <fieldset>
            <legend>Card Details</legend>
            <!-- Setting up the csrf topken in the hidden field-->
            <input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
            <p>
              <label class="labelWidth" for="name">Holder Name<span class="required_field"> *</span></label>
              <input type="text" name="name" required maxlength="25" size="25" value="<?php if(!empty($_POST['name'])){ echo esc_attr($_POST['name']); } ?>"/>
              <br />
            <?php if(!empty($errors['name'])) :?>
              <span class="show_error"><small><?=esc($errors['name']);?></small></span>
            <?php endif; ?>
            </p>
            <p>
              <label class="labelWidth" for="card_number">Holder Name<span class="required_field"> *</span></label>
              <input type="text" name="card_number" required maxlength="25" size="25" value="<?php if(!empty($_POST['card_number'])){ echo esc_attr($_POST['card_number']); } ?>"/>
              <br />
              <?php if(!empty($errors['card_number'])) :?>
                <span class="show_error"><small><?=esc($errors['card_number']);?></small></span>
              <?php endif; ?>
            </p>
            <p>
              <label class="labelWidth" for="exp_data">Expiry Date<span class="required_field"> *</span></label>
              <input type="text" name="exp_data" required maxlength="25" size="25" value="<?php if(!empty($_POST['exp_data'])){ echo esc_attr($_POST['exp_data']); } ?>"/>
              <br />
              <?php if(!empty($errors['exp_data'])) :?>
                <span class="show_error"><small><?=esc($errors['exp_data']);?></small></span>
              <?php endif; ?>
            </p>
            <p>
              <label class="labelWidth" for="cvv">CVV<span class="required_field"> *</span></label>
              <input type="text" name="cvv" required maxlength="25" size="25" value="<?php if(!empty($_POST['cvv'])){ echo esc_attr($_POST['cvv']); } ?>"/>
              <br />
              <?php if(!empty($errors['cvv'])) :?>
                <span class="show_error"><small><?=esc($errors['cvv']);?></small></span>
              <?php endif; ?>
            </p>
            <p id="formButton">
              <input type="submit" class="sign_up_formbutton" value="Complete Purchase" id="send_form"> &nbsp;&nbsp;
            </p>
          </fieldset>
        </form>
			</div>
			
		</div>

<?php
  include '../includes/footer.inc.php';
?>