<?php
/**File name: best_offers.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file for best offers desscription
*/
  $title = "Best Offers";
  require __DIR__.'/../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				<h1 id="offer_heading">Today's top 5 best offers</h1>
				<div id="offer_table">
					<table><!--Starting of table-->

						<caption style="font-size: 2.2rem;">Offers</caption><!--Table caption-->

						<tr><!--Starting of first row-->
							<th>S. No.</th>
							<th>Game:</th>
							<th>Description:</th>
							<th>Price:</th>
						</tr><!--ending of first frow-->

						<tr><!--Starting of second row-->
							<td>1.</td>
							<td><img src="images/ps4/uncharted1.jpg" alt="Uncharted 4" /></td>
							<td>
								<p>Name: Uncharted 4</p>
								<p>Platform: Playstation 4</p>
								<p>Genre: Action, Adventure</p>
							</td>
							<td>
								<p>Old Price: <span class="old_price">$68.99</span></p>
								<p>New Price: <span>$59.99</span></p>
								<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
							</td>
						</tr><!--Ending of second row-->

						<tr><!--Starting of third row-->
							<td>2.</td>
							<td><img src="images/pc/farcry5.jpg" alt="Farcry 5"/></td>
							<td>
								<p>Name: Farcry 5</p>
								<p>Platform: PC</p>
								<p>Genre: Action, FPS,  Adventure</p>
							</td>
							<td>
								<p>Old Price: <span class="old_price">$88.99</span></p>
								<p>New Price: <span>$69.99</span></p>
								<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
							</td>
						</tr><!--Ending of third row-->

						<tr><!--Starting of fourth row-->
							<td>3.</td>
							<td><img src="images/xbox/fortnite.jpg" alt="Fortnite"/></td>
							<td>
								<p>Name: Fortnite</p>
								<p>Platform: Xbox One</p>
								<p>Genre: Action, Adventure</p>
							</td>
							<td>
								<p>Old Price: <span class="old_price">$98.99</span></p>
								<p>New Price: <span>$79.99</span></p>
								<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
							</td>
						</tr><!--Ending of fourth row-->

						<tr><!--Starting of fifth row-->
							<td>4.</td>
							<!--Image Source https://images-na.ssl-images-amazon.com/images/I/91dOeY47ndL._SX342_.jpg -->
							<td><img src="images/ps4/The_Crew.jpg" alt="The Crew"/></td>
							<td>
								<p>Name: The Crew</p>
								<p>Platform: Playstation 4</p>
								<p>Genre: Racing</p>
							</td>
							<td>
								<p>Old Price: <span class="old_price">$68.99</span></p>
								<p>New Price: <span>$59.99</span></p>
								<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
							</td>
						</tr><!--Ending of fifth row-->

						<tr><!--Starting of sixth row-->
							<td>5.</td>
							<!--Image source http://i60.tinypic.com/qsmlbl.jpg -->
							<td><img src="images/pc/metro.jpg" alt="Metro"/></td>
							<td>
								<p>Name: Metro</p>
								<p>Platform: PC</p>
								<p>Genre: Action, Survival</p>
							</td>
							<td>
								<p>Old Price: <span class="old_price">$66.99</span></p>
								<p>New Price: <span>$59.99</span></p>
								<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
							</td>
						</tr><!--Ending of sixth row-->
					</table><!--Ening of table-->
				</div>
			</div>
			
		</div>

<?php
  include '../includes/footer.inc.php';
?>