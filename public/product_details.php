<?php
/**
* File name: product_details.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file to display the detailed view of desired product.
*/
  $title = "Details";
  require __DIR__.'/../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  include '../database/games_model.php';
  include 'validate.php';

  //variable to store the kind of request made on page.
  $kind = "";
  //check if session['add_success'] is set  
  if(isset($_SESSION['add_success'])) {
    //check if add_success message is set.
    if (isset($_SESSION['add_item_message'])) {
      $message = $_SESSION['add_item_message'];
      //unset the add_success message.
      unset($_SESSION['add_item_message']);
    }
      unset($_SESSION['add_success']);    
  }

  //check if session['add_success'] is set  
  if(isset($_SESSION['remove_success'])) {
    //check if add_success message is set.
    if (isset($_SESSION['remove_item_message'])) {
      $message = $_SESSION['remove_item_message'];
      //unset the add_success message.
      unset($_SESSION['remove_item_message']);
    }
      unset($_SESSION['remove_success']);    
  }
  //check if the request is the get request
  if(isset($_GET['product_id'])){
  	//set kind to regular
  	$kind = "regular";
  	$details = fetchProductDetails($dbh, $_GET['product_id']);
	$genre = fetchProductGenre($dbh, $_GET['product_id']);
	$platform = $_GET['platform'];
  }
  //check if the page request is Get and used called for searching
  if(isset($_GET['search'])){
  	//variable to check if the search is successfull or not
  	$success = false;
  	//set kind to search result
  	$kind = "search_result";
  	$search_term = $_GET['search'];
	//var_dump($search_term);
	$details = searchGames($dbh, $_GET['search']);
	$platform = searchedProductPlatform($dbh, $_GET['search']);

	if(!empty($details) && !empty($platform)){
		$success = true;
	}
	//var_dump($details);
	//var_dump($platform);
  }
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				

				<div id="game_detail">

					<?php if(isset($message)) : ?>
				        <div id="new_flash_message">
				          <p><?=$message;?></p>
				        </div>
				      <?php endif;?>

					<?php if($kind == "regular") : ?>

					<div id="game_image">
						<?php foreach($details as $row): ?>
							<img src="images/<?=esc_attr($platform);?>/<?=esc_attr($row['image']);?>.jpg" alt="<?=esc_attr($row['name']);?>" />
						<?php endforeach; ?>
					</div>
					<div id="game_info">
						<?php foreach ($details as $product) : ?>
							<h1><?=esc_attr($product['name'])?></h1>
							<small>Platforms</small>
							<h2><?=$platform ?></h2>
							<small>Genre</small>
							<h2>
							<?php foreach ($genre as $product_genre){
								echo esc_attr($product_genre['name']).", ";
							}
							?>
							</h2>
							<small>Price</small>
							<h2><?='$ '.$product['price']?></h2>
						
							<div id="cartbutton">
								<form action="cart.php" method="post">
	                				<input type="hidden" name="product_id" value="<?=esc_attr($product['product_id']);?>"/>
	                				<input type="hidden" name="name" value="<?=esc_attr($product['name']);?>"/>
	                				<input type="hidden" name="platform" value="<?=esc_attr($platform);?>"/>
	                				<input type="hidden" name="price" value="<?=esc_attr($product['price']);?>"/>
			            			<input type="submit" value="Add to Cart" />        
						    	</form>
					    	</div>
					    <?php endforeach; ?>
					</div>

							
					<div id="game_description">
						<h3>Details</h3>
						<?php foreach($details as $product) : ?>
							<p>Publisher: <?=esc_attr($product['publisher'])?></p>
							<p>Developer: <?=esc_attr($product['developer'])?></p>
							<p>Rating: <?=esc_attr($product['rating'])?></p>
							<p>Description: <?=esc_attr($product['description'])?></p>
						<?php endforeach;?>
					</div>

				

				<div id="similar_games">
					<?php
					    if(!empty($_SESSION['cart'])){
					      include '../includes/cart.inc.php';
					    }
				  	?>
				</div>

			<?php endif; ?>

			<?php if($kind == "search_result") :?>

				<div id="searched_product">
					

					<?php if($success) :?>
					<h1>Title: <?=esc_attr($details['name']); ?></h1>
					<p>Platforms: 
						<?php foreach($platform as $row) :?> 
							<span><a href="product_details.php?product_id=<?=esc_attr($details['product_id'])?>&platform=<?=esc_attr($row['platform_name'])?>"><?=esc_attr($row['platform_name']);?></a></span>
						<?php endforeach; ?>
					</p>
					<h2>Publisher: <?=esc_attr($details['publisher']); ?></h2>
					<h2>Price: $<?=esc_attr($details['price']); ?></h2>
					<?php else: ?>
						<h1>Sorry!! No Record Found by that title....</h1>
						<p style="text-align: center;"><span><a href="index.php">Home</a></span></p>
					<?php endif; ?>
				</div>

			<?php endif; ?>

			</div>
			
		</div>
</div>
<?php
  include '../includes/footer.inc.php';
?>