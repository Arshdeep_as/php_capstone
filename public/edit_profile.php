<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 20-08-2018
   * Time: 05:18 PM
   * Description: to edit profile by a user
   */
  $title = "Profile";
  $heading = "Create your Account";
  require __DIR__.'/../config.php';
  use \Classes\Utility\Validator;
  $validate = new Validator();
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require 'validate.php';
  include '../database/user_model.php';
  
  
  $id=$_SESSION['user_id'];   
  //getting user details from the database.
  $entered_details = fetchUserDetails($dbh, $id);
  //checking if the server request method is equals to the post.
  if($_SERVER['REQUEST_METHOD'] == 'POST'){

    //checking if the predefined csrf matches the csrf provided by the post request.
    if($_POST['site_csrf'] == $_SESSION['site_csrf']){

      //if user pressed the update button then, 
      $entered_details = $_POST;

      //validating the form fields to check if user entered the correct information
      $required_form_fields = ['first_name','last_name','street','city','postal_code','province','country','telephone','user_name'];
      $only_character_fields = ['first_name','last_name','city','province','country'];
      $validate->required($required_form_fields);
      $validate->onlyCharacters($only_character_fields);
      $validate->emailValidation('email_id');
      $validate->telephoneValidation('telephone');
      $validate->postalCodeValidation('postal_code');
      $validate->streetNameValidation('street');
      $errors = $validate->errors();
      //var_dump($errors);
      //if there are no errors in the data provided by the user, then we are going to update that data into the database.
      if(count($validate->errors()) == 0){
        try{
          //variable to store the boolean, if record is updated of not
          $isupdated = updateUserProfile($dbh, $entered_details);
          if($isupdated){
            header('Location:profile.php');
            die;
          }
          else{
            die('There is a problem while updating your profile');
          }
        }
        catch(PDOException $e) {
          print($e->getMessage());
          $sqlerror= $e->getMessage();
          if(!empty($sqlerror)){
            if(strpos($sqlerror,'email_id')){
            $errors['email_id']="Sorry, this email is already taken!";
            }
            elseif(strpos($sqlerror,'user_name')){
              $errors['user_name']="Sorry, this user name is already taken!";
            }
          }
        }        
      }//end if count errors.
    }//end csrf token match.
  }//end if server request method
  
?>
  
  <div id="content">
    <h1 style="text-align: center">EDIT Profile</h1>
    
    <div id="edit_profile">
      <form class="my_form" action="edit_profile.php" method="post" autocomplete="off" novalidate="novalidate">
        <fieldset>
          <legend>User Information</legend>
          <!-- Setting up the csrf topken in the hidden field-->
          <input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
          <p>
            <input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
            <input type="hidden" name="user_id" value="<?=$entered_details['user_id'];?>" />

          </p>
          <p>
            <label class="labelWidth" for="first_name">First Name<span class="required_field"> *</span></label>
            <input type="text" name="first_name" required maxlength="25" size="25" value="<?php if(!empty($entered_details['first_name'])){ echo esc_attr($entered_details['first_name']); } ?>"/>
            <br />
            <?php if(!empty($errors['first_name'])) :?>
              <span class="show_error"><small><?=esc($errors['first_name']);?></small></span>
            <?php endif; ?>
          </p>
          <p>
            <label class="labelWidth" for="last_name">Last Name<span class="required_field"> *</span></label>
            <input type="text" name="last_name" required maxlength="25" size="25" value="<?php if(!empty($entered_details['last_name'])){ echo esc_attr($entered_details['last_name']); } ?>"/>
            <br />
            <?php if(!empty($errors['last_name'])) :?>
              <span class="show_error"><small><?=esc($errors['last_name']);?></small></span>
            <?php endif; ?>
          </p>
          <p>
            <label class="labelWidth" for="street">Street<span class="required_field"> *</span></label>
            <input type="text" id="street" name="street" required maxlength="25" size="25" value="<?php if(!empty($entered_details['street'])){ echo esc_attr($entered_details['street']); } ?>" />
            <br />
            <?php if(!empty($errors['street'])) :?>
              <span class="show_error"><small><?=esc($errors['street']);?></small></span>
            <?php endif; ?>
          </p>
          <p>
            <label class="labelWidth" for="city">City<span class="required_field"> *</span></label>
            <input type="text" id="city" name="city" required maxlength="25" size="25" value="<?php if(!empty($entered_details['city'])){ echo esc_attr($entered_details['city']); } ?>" />
            <br />
            <?php if(!empty($errors['city'])) :?>
              <span class="show_error"><small><?=esc($errors['city']);?></small></span>
            <?php endif; ?>
          </p>
          
          <p>
            <label class="labelWidth" for="postal_code">Postal Code<span class="required_field"> *</span></label>
            <input type="text" id="postal_code" name="postal_code" required maxlength="25" size="25" value="<?php if(!empty($entered_details['postal_code'])){ echo esc_attr($entered_details['postal_code']); } ?>" />
            <br />
            <?php if(!empty($errors['postal_code'])) :?>
              <span class="show_error"><small><?=esc($errors['postal_code']);?></small></span>
            <?php endif; ?>
          </p>
          
          <p>
            <label class="labelWidth" for="province">Province<span class="required_field"> *</span></label>
            <input type="text" id="province" name="province" required maxlength="25" size="25" value="<?php if(!empty($entered_details['province'])){ echo esc_attr($entered_details['province']); } ?>" />
            <br />
            <?php if(!empty($errors['province'])) :?>
              <span class="show_error"><small><?=esc($errors['province']);?></small></span>
            <?php endif; ?>
          </p>
          <p>
            <label class="labelWidth" for="country">Country<span class="required_field"> *</span></label>
            <input type="text" id="country" name="country" required maxlength="25" size="25" value="<?php if(!empty($entered_details['country'])){ echo esc_attr($entered_details['country']); } ?>" />
            <br />
            <?php if(!empty($errors['country'])) :?>
              <span class="show_error"><small><?=esc($errors['country']);?></small></span>
            <?php endif; ?>
          </p>
          
          <p>
            <label class="labelWidth" for="telephone">Contact No.<span class="required_field"> *</span></label>
            <input type="tel" id="telephone" name="telephone" required maxlength="25" size="25" value="<?php if(!empty($entered_details['telephone'])){ echo esc_attr($entered_details['telephone']); } ?>" />
            <br />
            <?php if(!empty($errors['telephone'])) :?>
              <span class="show_error"><small><?=esc($errors['telephone']);?></small></span>
            <?php endif; ?>
          </p>
          
          <p>
            <label class="labelWidth" for="email_id">Email ID<span class="required_field"> *</span></label>
            <input type="email" id="email_id" name="email_id" required maxlength="25" size="25" value="<?php if(!empty($entered_details['email_id'])){ echo esc_attr($entered_details['email_id']); } ?>" />
            <br />
            <?php if(!empty($errors['email_id'])) :?>
              <span class="show_error"><small><?=esc($errors['email_id']);?></small></span>
            <?php endif; ?>
          </p>
   
          <p>
            <label class="labelWidth" for="user_name">User Name<span class="required_field"> *</span></label>
            <input type="text" id="user_name" name="user_name" required maxlength="25" size="25" value="<?php if(!empty($entered_details['user_name'])){ echo esc_attr($entered_details['user_name']); } ?>" />
            <br />
            <?php if(!empty($errors['user_name'])) :?>
              <span class="show_error"><small><?=esc($errors['user_name']);?></small></span>
            <?php endif; ?>
          </p>
          <p id="formButton">
            <input type="submit" class="formbutton" value="Update Profile" id="send_form">
          <p id="checkout_page"><a href="change_password.php">Change Password</a></p>
        </fieldset>
      </form>

      
    </div>
    
  </div>
  </div>
<?php
  include '../includes/footer.inc.php';
?>