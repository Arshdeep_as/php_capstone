<?php
/**
 *Description file to show the company information
 *@filename about_us.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */
  $title = "About Us";
  $heading = "Sorry, This website in not finished yet.";
  require __DIR__.'/../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
?>

			<div id="content" class="about">
       <h1>Company Details</h1>
        <p>ASR Web Solutions is created by Mr. Arshdeep Singh Rangi in 2010, as a small, creative web development and designing company in Winnipeg, Manitoba.</p><p> In years, this company had developed a reputation in building all kind of websites like service website, blog, celebrity website, crowdfunding website, dating website, community website, forum website etc.</p>
         
        <p>ASR Web Solution provide wide range of services including:</p>
        <ol>
          <li>Web design and development</li>
          <li>Search engine optimization</li>
          <li>Mobile app development</li>
          <li>Graphic design</li>
          <li>Domain registration and hosting</li>
          <li>Custom JavaScript programming</li>
          <li>Server-side scripting</li>
          <li>Database services and many more.</li>
        </ol>
        
        </div>
		</div>

<?php
  include '../includes/footer.inc.php';
?>