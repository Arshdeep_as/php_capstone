<?php
/**
 *Description
 *@filename pc_games.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */
  $title = "PC Games";
  require __DIR__.'/../config.php';
  use \Classes\Utility\Validator;
  $v = new Classes\Utility\Validator();
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require __DIR__ . '/../database/games_model.php';
  require 'validate.php';
  $platform = 'PC';
  $pc_games = fetchGames($dbh, $platform);
  
  //check if session['logout'] is set
  if(isset($_SESSION['empty_cart'])) {
    //check if logout message is set.
    if (isset($_SESSION['empty_cart_message'])) {
      $message = $_SESSION['empty_cart_message'];
      //unset the logout message.
      unset($_SESSION['empty_cart_message']);
    }
  }
  
  
?>
			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				
        <?php include('../includes/sidebar.inc.php');?>
        
				<div id="games_showcase">
					<h1><?=$title;?></h1>
					<?php if(isset($message)) : ?>
				        <div id="new_flash_message">
				          <p><?=$message;?></p>
				        </div>
				    <?php endif;?>
					<div id="gallery">
            <?php foreach($pc_games as $row) : ?>
						<div class="item1">
							<!--Image source http://www.dbzgames.org/files/images/games/1/2/136/cover.jpg -->
							<a href="#"><img src="images/pc/<?=esc_attr($row['image'])?>.jpg" alt="<?=esc_attr($row['name'])?>" /></a>
							<p><?=$row['name']?></p>
							<p>$<?=$row['price']?></p>
							<div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform_name'])?>"><span>Buy Now</span></a></div>
						</div>
            <?php endforeach; ?>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
		</div>
<?php
  include '../includes/footer.inc.php';
?> 