<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 20-08-2018
   * Time: 05:35 PM
   * Description: To logout the user
   */
  require __DIR__.'/../config.php';

  //To check if the user is logged in
  if(isset($_SESSION['user_id'])) {
    //unset the session variables to logoout and set the logout flash message.
    unset($_SESSION['user_id']);
    unset($_SESSION['user_name']);
    session_destroy();

    //change of privileges.
    session_regenerate_id();
    session_start();
    $_SESSION['logout']=true;
    $_SESSION['logout_msg']="You have successfully logged out!";
    header('Location:login.php');
    die;
  }