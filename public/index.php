<?php
/**
 *Description
 *@filename index.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */
  $title = "Capstone Project";
  $heading = "Best selling Games";
  require __DIR__.'/../config.php';
  use \Classes\Utility\Validator;
  $validate = new Classes\Utility\Validator();  
  require 'validate.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require __DIR__ . '/../database/games_model.php';
  
  //fetching rndom games for all platforms.
  $pc_games = fetchRandomGames($dbh, 'PC');
  $ps4_games = fetchRandomGames($dbh, 'PS 4');
  $xbox_games = fetchRandomGames($dbh, 'Xbox One');
  $upcoming_games = fetchUpcomingGames($dbh);

  
?>
			
			<main id="content"><!--Content Div Starts-->
				
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        		<h1 style="text-align: center"><?=$heading;?></h1>
        
				<div id="hero_div">
					<img src="images/hero_image/hero2.jpg" alt="Banner Image"/>
				</div>
        
    
				<div id="offers_div"><!--Offers Div Starts-->
					<div id="latest_games"><!--Latest games Div Starts-->
						<p class="tag1">Latest Games</p>
						<?php foreach($upcoming_games as $row) :?>
							<div class="latest_games_items">
							
								<a href="#"><img src="images/upcoming/<?=esc_attr($row['image']);?>.jpg" alt="<?=esc_attr($row['name']);?>" /></a>
								<p><?=$row['name'];?></p>
								<p>$<?=$row['price'];?></p>
								<div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform'])?>"><span>Buy Now</span></a></div>
							</div>
						<?php endforeach;?>
						<p><a href="pc_games.php">see all...</a></p>
					</div><!--Latest games Div Ends-->
					
					<div id="pc_games"><!--PC games Div Starts-->
						<p class="tag1">PC Games</p>


           				 <?php foreach($pc_games as $row) : ?>
            
						<div class="upcomming_games_items">
       
							<!--Image source https://i.pinimg.com/originals/00/fa/4e/00fa4e177df49279ac758c50a5487a72.jpg -->
							<a href="#"><img src="images/pc/<?=esc_attr($row['image']);?>.jpg" alt="<?=esc_attr($row['name']);?>" /></a>
							<p><?=$row['name'];?></p>
							<p>$<?=$row['price'];?></p>
							<div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform'])?>"><span>Buy Now</span></a></div>
						</div>
  
						<?php endforeach; ?>
      
						<p><a href="pc_games.php">see all...</a></p>
					</div><!--PC games Div Ends-->
					
					<div id="ps4_games"><!--PS4 games Div Starts-->
						<p class="tag1">Play Station 4 Games</p>

			            <?php foreach($ps4_games as $row) : ?>


			            <div class="latest_games_items">
							<a href="#"><img src="images/ps 4/<?=esc_attr($row['image']);?>.jpg" alt="<?=esc_attr($row['name']);?>" /></a>
							<p><?=$row['name'];?></p>
							<p>$<?=$row['price'];?></p>
							<div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform'])?>"><span>Buy Now</span></a></div>
						</div>
  
            <?php endforeach; ?>
						
						<p><a href="pc_games.php">see all...</a></p>
					</div><!--PS4 games Div ends-->
					
					<div id="xbox_games"><!--Xbox1 games Div Starts-->
						<p class="tag1">Xbox 1 Games</p>
            <?php foreach($xbox_games as $row) : ?>
						<div class="upcomming_games_items">
							<a href="#"><img src="images/xbox one/<?=esc_attr($row['image']);?>.jpg" alt="<?=esc_attr($row['name']);?>" /></a>
							<p><?=$row['name'];?></p>
							<p>$<?=$row['price'];?></p>
							<div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform'])?>"><span>Buy Now</span></a></div>
						</div>
						<?php endforeach; ?>
						<p><a href="pc_games.php">see all...</a></p>
					</div>	<!--Xbox1 games Div ends-->				
				</div><!--Offers Div Ends-->
				
				<!--<div id="callout">
					<p>Don't have an account yet...?&nbsp;&nbsp;&nbsp;Create your account Now...</p>
					<div class="button"><a href="sign_up.html"><span>Sign up</span></a></div>
				</div>-->
				
			</main><!--Content Div Ends-->
			<div class="clearfix"></div>
		</div><!--Wrapper Div Starts-->
		
<?php
  include '../includes/footer.inc.php';
?>