<?php
/**File name: checkout.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file for checkout page to insert into invoice table
*/
  $title = "Thank You";
  require __DIR__.'/../config.php';
  
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  include '../database/games_model.php';
  include '../database/user_model.php';
  require 'validate.php';
  if(!empty($_SESSION['cart'])){
    $product_id = $_SESSION['cart'];

    $invoice_id = $_SESSION['invoice_id'];
    $details=[];
    $sub_total = 0;
   /* for($i=0; $i<count($product_id); $i++){
      //var_dump($product_id[$i]);
      $details[$i] = fetchProductDetails3($dbh, $product_id[$i]);
      //$platform[$i] = fetchPlatforms($dbh, $product_id[$i]);
    }
*/


    $details=$_SESSION['cart'];
     
    $invoice_data = getInvoiceDetails($dbh, $invoice_id);
    $user_details = fetchUserDetails($dbh, $invoice_data['user_id']);

    unset($_SESSION['cart']);
  }
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        <div id="thankyou">
  				<h1>Thank You</h1>

          <h2>Your Detailed official Invoice Is: </h2>
          <h3>Invoice Number: <?=esc_attr($invoice_data['invoice_id']);?></h3>
          <h3>Invoice Date: <?=esc_attr($invoice_data['invoice_date']);?></h3>

          <h3>Shipping Address: </h3>
          <p>Name: <?=esc_attr($user_details['first_name'])?>  <?=esc_attr($user_details['last_name']);?></p>
          <p><strong>Address:</strong></p>
          <p> <?=esc_attr($user_details['street'])?>, <?=esc_attr($user_details['city']);?></p>
          <p><p><?=esc_attr($user_details['postal_code'])?>  <?=esc_attr($user_details['province']);?></p>
          <h3>Payment Status : Complete</h3>
          <table>
            <tr>
              
              <th>Name</th>
              <th>Platform</th>
              <th>Price</th>
            </tr>

            <?php foreach($details as $row) :?>
              <tr>
                
                <td><?=esc_attr($row['name'])?></td>
                <td><?=esc_attr($row['platform']);?></td>
                <td>$<?=esc_attr($row['price'])?></td>
              </tr>
            <?php endforeach; ?>

            <tr>
              <td colspan="2">Sub Total</td>
              <td><strong>$<?=esc_attr($invoice_data['sub_total']);?></strong></td>
            </tr>

            <tr>
              <td colspan="2">PST</td>
              <td>$<?=esc_attr($invoice_data['pst']);?></td>
            </tr>

            <tr>
              <td colspan="2">GST</td>
              <td>$<?=esc_attr($invoice_data['gst']);?></td>
            </tr>

            <tr>
              <td colspan="2">Total</td>
              <td><strong>$<?=esc_attr($invoice_data['total']);?></strong></td>
            </tr>
          </table>
        </div>
			</div>
			
		</div>

<?php
  include '../includes/footer.inc.php';
?>