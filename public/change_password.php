<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 20-08-2018
   * Time: 05:18 PM
   * Description: php file to change user password
   */
  $title = "Profile";
  require __DIR__.'/../config.php';
  use \Classes\Utility\Validator;
  $validate = new Validator();
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require 'validate.php';
  include '../database/user_model.php';
  
  
  $id=$_SESSION['user_id'];   
  
  //checking if the server request method is equals to the post.
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //if user pressed the update button then, 
    $entered_details = $_POST;

    //validating the form fields to check if user entered the correct information
    $required_form_fields = ['password','new_password','confirm_new_password'];
    $validate->required($required_form_fields);
    $validate->passwordValidation('new_password', 'confirm_new_password');
    $errors = $validate->errors();
    var_dump($errors);
    //if there are no errors in the data provided by the user, then we are going to update that data into the database.
      if(count($validate->errors()) == 0){
        //get old password from the db.
        $old_password = getOldPassword($dbh, $id);
        //if entered old password is correct
        if(password_verify($_POST['password'], $old_password['password'])){

          $isPasswordUpdated = updatePassword($dbh, $id, $_POST['new_password']);
          if($isPasswordUpdated){
            header('Location:logout.php');
            die;
          }
          else{
            die('There is a problem while changing your password');
          }

        }
        else{
          die('sorry old password is wrong');
        }
      }//end if count errors.
      
  }//end if server request method
  
?>
  
  <div id="content">
    <h1 style="text-align: center">EDIT Profile</h1>
    
    <div id="edit_profile">
      <form class="my_form" action="change_password.php" method="post" autocomplete="off" novalidate="novalidate">
        <fieldset>
          <legend>Change Password</legend>
            <p>
              <input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
              <input type="hidden" name="user_id" value="<?=$id;?>" />
            </p>
            <p>
              <label class="labelWidth" for="password">Current Password<span class="required_field"> *</span></label>
              <input type="password" id="password" name="password" required maxlength="25" size="25"  />
              <br />
              <?php if(!empty($errors['password'])) :?>
                <span class="show_error"><small><?=esc($errors['password']);?></small></span>
              <?php endif; ?>
            </p>
            <p>
              <label class="labelWidth" for="new_password">New Password<span class="required_field"> *</span></label>
              <input type="password" id="new_password" name="new_password" required maxlength="25" size="25"  />
              <br />
              <?php if(!empty($errors['new_password'])) :?>
                <span class="show_error"><small><?=esc($errors['new_password']);?></small></span>
              <?php endif; ?>
            </p>
            <p>
              <label class="labelWidth" for="confirm_new_password">Confirm New Password<span class="required_field"> *</span></label>
              <input type="password" id="confirm_new_password" name="confirm_new_password" required maxlength="25" size="25"  />
              <br />
              <?php if(!empty($errors['confirm_new_password'])) :?>
                <span class="show_error"><small><?=esc($errors['confirm_new_password']);?></small></span>
              <?php endif; ?>
            </p>
            <p id="formButton">
            <input type="submit" class="sign_up_formbutton" value="Change Password" id="send_form"> &nbsp;&nbsp;
          </p>
        </fieldset>
      </form>

      
    </div>
    
  </div>
  </div>
<?php
  include '../includes/footer.inc.php';
?>