<?php
  /**
   *Description file to show the website is not finished yet
   *@filename sorry_message.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
  $title = "Sign Up";
  $heading = "Sorry";
  require __DIR__.'/../config.php';
  //var_dump(APP);
  use \Classes\Utility\Validator;
  $v = new Classes\Utility\Validator();
  //var_dump($v);
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
?>
			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        <h1><?=$heading;?></h1>
				<p>This website in not finished yet.</p>
				
			</div>
			
		</div>
<?php
  include '../includes/footer.inc.php';
?>