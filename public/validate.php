<?php
   /**
   *Description file to store functions regading validation.
   *@filename sorry_message.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
  
  
  function esc($string)
  {
    return htmlspecialchars($string, NULL, 'UTF-8', false);
  }
  
  function esc_attr($string)
  {
    return htmlentities($string, ENT_QUOTES, 'UTF-8', false);
  }
  