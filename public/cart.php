<?php
  /**
   *Description file to store cart regarding information.
   *@filename sorry_message.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
  $title = "Cart";
  require __DIR__.'/../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  include '../database/games_model.php';
  //include '../database/cart.model.php';

  //Make sure we have a post request.
  if(filter_input(INPUT_SERVER, 'REQUEST_METHOD') != 'POST'){
    die('Sorry, you are in a wrong place.');
  }

  //make sure there is  cart or make one.
  if(!isset($_SESSION['cart'])){
    $_SESSION['cart'] = [];    
  }
  
  if(!empty($_POST['product_id'])&& !empty($_POST['platform'])){
    //addToCart($dbh, $_POST['product_id']);
    //var_dump($_POST['product_id']);
    $id=$_POST['product_id'];
    $platform = $_POST['platform'];
    $price=$_POST['price'];
    $name=$_POST['name'];
    $_SESSION['cart'][$id]=[
      'product_id'=>$id,
      'platform'=>$platform,
      'price'=>$price,
      'name'=>$name
    ];

    $_SESSION['add_success'] = true;
    $_SESSION['add_item_message'] = 'Item added to your cart.';
       
    header('Location:'.$_SERVER['HTTP_REFERER']);
    die;
  }
  
?>