<?php
/**File name: search_form.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: server file to return response to ajax response.
*/

//check if the server request method is post
if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $term = $_POST['search'];
    //print_r($term);
    
    if(!empty($term)){
      //connect to database
      $dbh = new PDO('mysql:host=localhost;dbname=gaming_db', 'root', '');
      //set error mode on
      $dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      //create query
  		$query = "SELECT product_id, name FROM product WHERE name LIKE CONCAT(:term, '%')";
      //prepare statement
  		$stmt = $dbh -> prepare($query);
      //bind values
      $stmt -> bindValue(':term', $term, PDO::PARAM_STR);
      //execute statement
  		$stmt -> execute();

      //fetch results
  		$product = $stmt -> fetchAll(PDO::FETCH_ASSOC);
  	 
      //set header type to application json
      header('Content-type: application/json');
      //echo out the results as json encoded result
      echo json_encode($product);
    }

 }