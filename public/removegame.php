<?php

/**
* File name: removegame.php
* Author: Arshdeep singh
* Date: 18-08-2018
* Description: php file to display the detailed view of desired product.
*/
  
  require __DIR__.'/../config.php';
  
  include '../database/games_model.php';
  include 'validate.php';

$id=$_GET['id'];

unset($_SESSION['cart'][$id]);
$_SESSION['remove_success'] = true;
$_SESSION['remove_item_message'] = 'Item removed from your cart.';
header("Location: {$_SERVER['HTTP_REFERER']}");