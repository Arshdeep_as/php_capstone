<?php
/**
 *@filename ps4_games.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 *@description page to show the list view of ps4 products stored in db.
 */
  $title = "PS4 Games";
  $heading = "Playstation 4 Games";
  require __DIR__.'/../config.php'; 
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require __DIR__ . '/../database/games_model.php';
  require 'validate.php';
  //defining a platform variable to store platform
  $platform = 'PS 4';
  //fetching games as per pertform defined
  $ps4_games = fetchGames($dbh, $platform);
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        
        <?php include('../includes/sidebar.inc.php');?>
				
				<div id="games_showcase">
					<h1><?=$heading;?></h1>
					<div id="gallery">
            <?php foreach($ps4_games as $row) : ?>
              <div class="item1">
                <!--Image source http://www.dbzgames.org/files/images/games/1/2/136/cover.jpg -->
                <a href="#"><img src="images/ps 4/<?=esc_attr($row['image'])?>.jpg" alt="<?=esc_attr($row['name'])?>" /></a>
                <p><?=$row['name']?></p>
                <p>$<?=$row['price']?></p>
                <div class="button"><a href="product_details.php?product_id=<?=esc_attr($row['product_id'])?>&platform=<?=esc_attr($row['platform_name'])?>"><span>Buy Now</span></a></div>
              </div>
            <?php endforeach; ?>
						
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
		</div>

<?php
  include '../includes/footer.inc.php';
?>