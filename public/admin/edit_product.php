<?php
/**
 * File edit_product.php
 * Description php file to update any entered record
 * @author  Arshdeep Singh Rangi
 */
  $title = "Edit Product";
  require __DIR__.'/../../config.php';  
  include __DIR__.'/../../includes/header.inc.php';
  include __DIR__.'/../../database/admin_model.php';
  include __DIR__.'/../validate.php';
  //validator class, storing all the validation functions.
  use \Classes\Utility\Validator;
  //making a new object of validator class
  $validate = new Validator();
 
  //include '../includes/nav.inc.php';
  
  //checking if the user is logged in as admin or not. if not then send them to the login page.
  if(!isset($_SESSION['admin'])){
  	$_SESSION['not_admin'] = true;
  	$_SESSION['not_admin_message'] = 'Sorry, you need to login as admin first.';
  	header('Location:../login.php');
  	die;
  }

  $relation = getAllRelations($dbh);
  
  //check if page is called by get request
  if(!empty($_GET['product_id'])){ 
  	$product = getProductDetails($dbh, $_GET['product_id']);
  }
  else{
  	$product = $_POST;
  }

  //array to hold the values of errors.
  $errors = [];
  //initializing the success flag
  $success = false;
  //checking if the sserver request method is post.
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
  	//checking if the predefined csrf matches the csrf provided by the post request.
  	if($_POST['site_csrf'] == $_SESSION['site_csrf']){
	  	// array containg the lsit of required fields.
	  	$required_form_fields = ['name','publisher','developer','price','image','thumbnail'];
	  	//checking if user filled all the required fields.
	  	$validate->required($required_form_fields);
	  	//validating fields for only chracters, number and spaces.
	  	$validate->nameValidation('name');
	  	$validate->nameValidation('publisher');
	  	$validate->nameValidation('developer');
	  	$validate->nameValidation('image');
	  	$validate->nameValidation('thumbnail');
	  	//validating fields to contain only numbers.
	  	$validate->numberValidation('quantity');
	  	//validation fields to contain only floating point number upto two places.
	  	$validate->floatValidation('price');
	  	//$errors array to store the errors which are comming.
	  	$errors = $validate->errors();

	  	//if there are no errors in the data provided by the admin, then we are going to update that record in the database.
	    if(count($validate->errors()) == 0){

	    	//variable to check if the product is updated in the database.
	    	$isupdated = updateProduct($dbh, $_POST);
	    	//if it is updated then setting the session variables to show flash messages.
	    	if($isupdated){
	    		$_SESSION['edit_product'] = true;
	    		$_SESSION['edit_message'] = "Product Edited";
	        	header('Location: table.php?relation=product');
	        	die;
	        }
	        else{
	        	//if the execute query for update record doesn't work, this means data is not updated in the database.
	          	die('There was problem in updating the record');
	        }
	    }//end if (count errors = 0)
	  }//end if csrf checling
	  else{
	  	die('Sorry, Nice try but you failed.');
	  }
  }// end if(server request method)

  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				<div id="detail_form">
					<h1>Edit Product</h1>
					<form id="myform" method="post" action="edit_product.php" autocomplete="off" novalidate>
						<fieldset>
							<legend>Product Information</legend>
							<!-- Setting up the csrf topken in the hidden field-->
              				<input type="hidden" name="site_csrf" value="<?=esc_attr($_SESSION['site_csrf']);?>" />
							<p><input type="hidden" name="product_id" value="<?=esc_attr($product['product_id'])?>"></p>
							<p>
								<label for="name">Product Name</label>
								<input type="text" name="name" value="<?=esc_attr($product['name'])?>" />
								<br />
				                <?php if(!empty($errors['name'])) :?>
				                  <span class="show_error"><small><?=esc($errors['name']);?></small></span>
				                <?php endif; ?>
							</p>
							<p>
								<label for="publisher">Publisher</label>
								<input type="text" name="publisher" value="<?=esc_attr($product['publisher'])?>" />
								<br />
				                <?php if(!empty($errors['publisher'])) :?>
				                  <span class="show_error"><small><?=esc($errors['publisher']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="developer">Developer</label>
								<input type="text" name="developer" value="<?=esc_attr($product['developer'])?>" />
								<br />
				                <?php if(!empty($errors['developer'])) :?>
				                  <span class="show_error"><small><?=esc($errors['developer']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="price">Price</label>
								<input type="text" name="price" value="<?=esc_attr($product['price'])?>" />
								<br />
				                <?php if(!empty($errors['price'])) :?>
				                  <span class="show_error"><small><?=esc($errors['price']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="image">Image Name</label>
								<input type="text" name="image" value="<?=esc_attr($product['image'])?>" />
								<br />
				                <?php if(!empty($errors['image'])) :?>
				                  <span class="show_error"><small><?=esc($errors['image']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="thumbnail">Thumbnail Name</label>
								<input type="text" name="thumbnail" value="<?=esc_attr($product['thumbnail'])?>" />
								<br />
				                <?php if(!empty($errors['thumbnail'])) :?>
				                  <span class="show_error"><small><?=esc($errors['thumbnail']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="in_stock">In Stock</label>
								<select name="in_stock">
								    <option <?php if($product['in_stock'] == 1){echo "selected";}?> value="1">Yes</option>
								    <option <?php if($product['in_stock'] == 0){echo "selected";}?> value="0">No</option>
								</select>
							</p>
							<p>
								<label for="quantity">Quantity</label>
								<input type="text" name="quantity" value="<?=esc_attr($product['quantity'])?>" />
								<br />
								<?php if(!empty($errors['quantity'])) :?>
				                  <span class="show_error"><small><?=esc($errors['quantity']);?></small></span>
				                <?php endif; ?>
							</p>
							<p>
								<label for="rating">Rating</label>
								<input type="hidden" name="rating_id" value="<?=esc_attr($product['rating_id'])?>">
								<select name="rating">
								  <option <?php if($product['rating_id'] == 1){echo "selected";}?> value="1">Mature</option>
							    <option <?php if($product['rating_id'] == 2){echo "selected";}?>value="2">13+</option>
							    <option <?php if($product['rating_id'] == 3){echo "selected";}?>value="3">17+</option>
							    <option <?php if($product['rating_id'] == 4){echo "selected";}?>value="4">18+</option>
							    <option <?php if($product['rating_id'] == 5){echo "selected";}?>value="5">Rating Pending</option>
								</select>
								<br />
				                <?php if(!empty($errors['description'])) :?>
				                  <span class="show_error"><small><?=esc($errors['description']);?></small></span>
				                <?php endif; ?>	
							</p>
							<p>
								<label for="supplier">Supplier</label>
								<input type="hidden" name="supplier_id" value="<?=esc_attr($product['supplier_id'])?>">
								<select name="supplier">
								  <option <?php if($product['supplier_id'] == 1){echo "selected";}?> value="1">Vip Outlet Winipeg</option>
							    <option <?php if($product['supplier_id'] == 2){echo "selected";}?> value="2">Jack Stores</option>
							    <option <?php if($product['supplier_id'] == 3){echo "selected";}?> value="3">Miller</option>
							    <option <?php if($product['supplier_id'] == 4){echo "selected";}?> value="4">Top Games</option>
							    <option <?php if($product['supplier_id'] == 5){echo "selected";}?> value="5">Extream Heat</option>
							    <option <?php if($product['supplier_id'] == 6){echo "selected";}?> value="6">Dead Zone</option>
							    <option <?php if($product['supplier_id'] == 7){echo "selected";}?> value="7">Furt X</option>
							    <option <?php if($product['supplier_id'] == 8){echo "selected";}?> value="8">Diesel</option>
							    <option <?php if($product['supplier_id'] == 9){echo "selected";}?> value="9">Devil Co.</option>
								</select>
								<br />
				                <?php if(!empty($errors['rating'])) :?>
				                  <span class="show_error"><small><?=esc($errors['rating']);?></small></span>
				                <?php endif; ?>
							</p>
							<p><input class="admin_buttons" type="submit" name="submit" value="Update" /></p>
						</fieldset>
					</form>
				</div>
			</div>
			
		</div>

<?php
  include '../../includes/footer.inc.php';
?>