<?php
/**
 * File edit_product.php
 * Description php file to add a new product to the database.
 * @author  Arshdeep Singh Rangi
 */
  $title = "Add Product";
  require __DIR__.'/../../config.php';  
  include __DIR__.'/../../includes/header.inc.php';
  include __DIR__.'/../../database/admin_model.php';
  include __DIR__.'/../validate.php';
  //validator class, storing all the validation functions.
  use \Classes\Utility\Validator;
  //making a new object of validator class
  $validate = new Validator();
 
  //checking if the user is logged in as admin or not. if not then send them to the login page.
  if(!isset($_SESSION['admin'])){
  	$_SESSION['not_admin'] = true;
  	$_SESSION['not_admin_message'] = 'Sorry, you need to login as admin first.';
  	header('Location:../login.php');
  	die;
  }

  $relation = getAllRelations($dbh);
  
  //array to hold the values of errors.
  $errors = [];
  //initializing the success flag
  $success = false;

  //checking if the user has submitted the form
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
  	//checking if the predefined csrf matches the csrf provided by the post request.
  	if($_POST['site_csrf'] == $_SESSION['site_csrf']){
	  	$rating_name = getRatingName($dbh, $_POST['rating_id']);
	  	$supplier_name = getSupplierName($dbh, $_POST['supplier_id']);
	  	//array containing the field names of all the required form fields.
	  	$required_form_fields = ['product_sku','name','publisher','developer','price','image','thumbnail','quantity','description'];
	  	//validatingthe fields to check they are filled
	  	$validate->required($required_form_fields);
	  	//validating fields for only chracters, number and spaces.
	  	$validate->nameValidation('name');
	  	$validate->nameValidation('publisher');
	  	$validate->nameValidation('developer');
	  	$validate->nameValidation('image');
	  	$validate->nameValidation('thumbnail');
	  	//validating fields to contain only numbers.
	  	$validate->numberValidation('product_sku');
	  	$validate->numberValidation('quantity');
	  	//validation fields to contain only floating point number upto two places.
	  	$validate->floatValidation('price');
	  	//$errors array to store the error messages.
	  	$errors = $validate->errors();
	  	//if there are no errors in the data provided by the admin, then we are going to update that record in the database.
	    if(count($validate->errors()) == 0){
	    	
	    	$isinserted = insertIntoProduct($dbh, $_POST);
	    	//variable to check if the product is inserted or not
	    	if($isinserted){
	    		$_SESSION['add_product'] = true;
	    		$_SESSION['add_message'] = "Product Added";
	        	header('Location: table.php?relation=product');
	        	die;
	        }
	        else{
	        	//if the execute query for update record doesn't work, this means data is not updated in the database.
	          	die('There was problem in updating the record');
	        }
	    }//end if (count errors = 0)*/
		}//end if csrf checling
	  else{
	  	die('Sorry, Nice try but you failed.');
	  }
  }// end if(server request method)

  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				<div id="detail_form">
					<h1>Add Product</h1>
					<form id="myform" method="post" action="add_product.php" novalidate>
						<fieldset>
							<legend>Product Information</legend>
							<!-- Setting up the csrf topken in the hidden field-->
              				<input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
							<p><input type="hidden" name="product_id" value=""></p>
							<p>
								<label for="product_sku">Product SKU</label>
								<input type="text" name="product_sku" value="<?php if(!empty($_POST['product_sku'])){ echo esc_attr($_POST['product_sku']); } ?>" />
								<br />
				                <?php if(!empty($errors['product_sku'])) :?>
				                  <span class="show_error"><small><?=esc($errors['product_sku']);?></small></span>
				                <?php endif; ?>
							</p>
							<p>
								<label for="name">Product Name</label>
								<input type="text" name="name" value="<?php if(!empty($_POST['name'])){ echo esc_attr($_POST['name']); } ?>" />
								<br />
				                <?php if(!empty($errors['name'])) :?>
				                  <span class="show_error"><small><?=esc($errors['name']);?></small></span>
				                <?php endif; ?>
							</p>
							<p>
								<label for="publisher">Publisher</label>
								<input type="text" name="publisher" value="<?php if(!empty($_POST['publisher'])){ echo esc_attr($_POST['publisher']); } ?>" />
								<br />
				                <?php if(!empty($errors['publisher'])) :?>
				                  <span class="show_error"><small><?=esc($errors['publisher']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="developer">Developer</label>
								<input type="text" name="developer" value="<?php if(!empty($_POST['developer'])){ echo esc_attr($_POST['developer']); } ?>" />
								<br />
				                <?php if(!empty($errors['developer'])) :?>
				                  <span class="show_error"><small><?=esc($errors['developer']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="price">Price</label>
								<input type="text" name="price" value="<?php if(!empty($_POST['price'])){ echo esc_attr($_POST['price']); } ?>" />
								<br />
				                <?php if(!empty($errors['price'])) :?>
				                  <span class="show_error"><small><?=esc($errors['price']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="image">Image Name</label>
								<input type="text" name="image" value="<?php if(!empty($_POST['image'])){ echo esc_attr($_POST['image']); } ?>" />
								<br />
				                <?php if(!empty($errors['image'])) :?>
				                  <span class="show_error"><small><?=esc($errors['image']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="thumbnail">Thumbnail Name</label>
								<input type="text" name="thumbnail" value="<?php if(!empty($_POST['thumbnail'])){ echo esc_attr($_POST['thumbnail']); } ?>" />
								<br />
				                <?php if(!empty($errors['thumbnail'])) :?>
				                  <span class="show_error"><small><?=esc($errors['thumbnail']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="in_stock">In Stock</label>
								<select name="in_stock">
								    <option <?php if(isset($_POST['in_stock'])){if($_POST['in_stock'] == 1){echo "selected";}}?> value="1">Yes</option>
								    <option <?php if(isset($_POST['in_stock'])){if($_POST['in_stock'] == 0){echo "selected";}}?> value="0">No</option>
								</select>
							</p>
							<p>
								<label for="quantity">Quantity</label>
								<input type="text" name="quantity" value="<?php if(!empty($_POST['quantity'])){ echo esc_attr($_POST['quantity']); } ?>" />
								<br />
								<?php if(!empty($errors['quantity'])) :?>
				                  <span class="show_error"><small><?=esc($errors['quantity']);?></small></span>
				                <?php endif; ?>
							</p>
							<p>
								<label for="description">Description</label>
								<input type="text" name="description" value="<?php if(!empty($_POST['description'])){ echo esc_attr($_POST['description']); } ?>" />
								<br />
				                <?php if(!empty($errors['description'])) :?>
				                  <span class="show_error"><small><?=esc($errors['description']);?></small></span>
				                <?php endif; ?>								
							</p>
							<p>
								<label for="rating_id">Rating</label>
								<!--<input type="hidden" name="rating_id" value="<?php if(!empty($_POST['rating'])){ echo esc_attr($_POST['rating']); } ?>">-->
								<select name="rating_id">
							    <option <?php if(isset($_POST['rating_id'])){if($_POST['rating_id'] == '1'){echo "selected";}}?> value="1">Mature</option>
							    <option <?php if(isset($_POST['rating_id'])){if($_POST['rating_id'] == '2'){echo "selected";}}?> value="2">13+</option>
							    <option <?php if(isset($_POST['rating_id'])){if($_POST['rating_id'] == '3'){echo "selected";}}?> value="3">17+</option>
							    <option <?php if(isset($_POST['rating_id'])){if($_POST['rating_id'] == '4'){echo "selected";}}?> value="4">18+</option>
							    <option <?php if(isset($_POST['rating_id'])){if($_POST['rating_id'] == '5'){echo "selected";}}?> value="5">Rating Pending</option>
								</select>
								<br />
				                <?php if(!empty($errors['rating_id'])) :?>
				                  <span class="show_error"><small><?=esc($errors['rating_id']);?></small></span>
				                <?php endif; ?>	
							</p>
							<p>
								<label for="supplier_id">Supplier</label>
								<!--<input type="hidden" name="supplier_id" value="<?php if(!empty($_POST['supplier'])){ echo esc_attr($_POST['supplier']); } ?>">-->
								<select name="supplier_id">
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '1'){echo "selected";}}?> value="1">Vip Outlet Winipeg</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '2'){echo "selected";}}?> value="2">Jack Stores</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '3'){echo "selected";}}?> value="3">Miller</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '4'){echo "selected";}}?> value="4">Top Games</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '5'){echo "selected";}}?> value="5">Extream Heat</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '6'){echo "selected";}}?> value="6">Dead Zone</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '7'){echo "selected";}}?> value="7">Furt X</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '8'){echo "selected";}}?> value="8">Diesel</option>
							    <option <?php if(isset($_POST['supplier_id'])){if($_POST['supplier_id'] == '9'){echo "selected";}}?> value="9">Devil Co.</option>
								</select>
								<br />
				                <?php if(!empty($errors['supplier_id'])) :?>
				                  <span class="show_error"><small><?=esc($errors['supplier_id']);?></small></span>
				                <?php endif; ?>	
							</p>
							<p><input class="admin_buttons" type="submit" name="submit" value="Add Product"></p>
						</fieldset>
					</form>
				</div>
			</div>
			
		</div>

<?php
  include '../../includes/footer.inc.php';
?>