<?php  
/**
 * File table.php
 * Description php file to show table contents, all of their data
 * @author  Arshdeep Singh Rangi
 */
  $title = "Admin";
  require __DIR__.'/../../config.php';  
  include __DIR__.'/../../includes/header.inc.php';
  include __DIR__.'/../../database/admin_model.php';
  include __DIR__.'/../validate.php';
  //include '../includes/nav.inc.php';
  
  //checking if the user is logged in as admin or not. if not then send them to the login page.
  if(!isset($_SESSION['admin'])){
  	$_SESSION['not_admin'] = true;
  	$_SESSION['not_admin_message'] = 'Sorry, you need to login as admin first.';
  	header('Location:../login.php');
  	die;
  }

  $relation = getAllRelations($dbh);	
	//checking if the table.php is called by get request to show database table	  
  if(isset($_GET['relation'])){
  	$table_name = $_GET['relation'];
  	$table_contents = getAlldetails($dbh, $_GET['relation']);
  }
	//checking if the table.php is called by get request to perform search
	else if(isset($_GET['search'])){
		$table_name = "product";
		$table_contents = searchProduct($dbh, $_GET['search']);
	}

	//checking the session variables to show the flash messages.
	//to show if the product is edited successfully.
	if(isset($_SESSION['edit_product'])){
		if(isset($_SESSION['edit_message'])){
			$flash = $_SESSION['edit_message'];
			unset($_SESSION['edit_product']);
			unset($_SESSION['edit_message']);
		}
	}	
  
  //to show if the product is deleted successfully.
	if(isset($_SESSION['delete_product'])){
		if(isset($_SESSION['delete_message'])){
			$flash = $_SESSION['delete_message'];
			unset($_SESSION['delete_product']);
			unset($_SESSION['delete_message']);
		}
	}	

	//to show if the product is added successfully.
	if(isset($_SESSION['add_product'])){
		if(isset($_SESSION['add_message'])){
			$flash = $_SESSION['add_message'];
			unset($_SESSION['add_product']);
			unset($_SESSION['add_message']);
		}
	}	

?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				
				<div id="relation">
					<a class="live_site" href="../index.php">Live Site</a>
					<h2>Tables</h2>
					<ul>
					<?php foreach($relation as $item) : ?>
						<li><a href="table.php?relation=<?=esc_attr($item['Tables_in_gaming_db'])?>"><?=esc_attr($item['Tables_in_gaming_db']) ?></a></li>
					<?php endforeach;?>
					</ul>
				</div>
				<div id="details">
					<?php if(isset($flash)) :?>
				  	<!--<span id="flash"><?=$flash?></span>-->
				  	<div id="flash_message_admin">
          		<?=$flash;?>
        		</div>
					<?php endif; ?>
				  <?php if($table_name == 'product') :?>
						
						<h1><?=esc_attr(ucfirst($table_name))?> Table Data</h1>

						<div id="add_product">
							<p><a href="add_product.php">Add Product</a></p>
						</div>
						
						<div id="table_container">
							<table>
								<caption><?=esc_attr(ucfirst($table_name))?></caption>
								<tr>
									<th>ACTION</th>
									<th>product_id</th>
									<th>name</th>
									<th>publisher</th>
									<th>developer</th>
									<th>price</th>
									<th>image</th>
									<th>quantity</th>
									<th>in_stock</th>
									<th>rating</th>
									<th>supplier</th>								
								</tr>
								
								<?php foreach($table_contents as $row) :?>
									<tr>		
										<td><a id="green" href="edit_product.php?product_id=<?=esc_attr($row['product_id']);?>">Edit</a><a id="red" href="delete.php?product_id=<?=esc_attr($row['product_id']);?>">Delete</a></td>
											
										<td><?=esc_attr($row['product_id']);?></td>
										<td><?=esc_attr($row['name']);?></td>
										<td><?=esc_attr($row['publisher']);?></td>
										<td><?=esc_attr($row['developer']);?></td>
										<td><?=esc_attr($row['price']);?></td>
										<td><?=esc_attr($row['image']);?></td>
										<td><?=esc_attr($row['quantity']);?></td>
										<td><?=esc_attr($row['in_stock']);?></td>
										<td><?=esc_attr($row['rating']);?></td>
										<td><?=esc_attr($row['supplier']);?></td>
									</tr>		
								<?php endforeach; ?>
							</table>
						</div>
					
					<?php else : ?>
						<p>Sorry, this page is under construction.....</p>
					<?php endif; ?>
				</div>
				<div id="clearfix"></div>
			</div>
		</div>

<?php
  include '../../includes/footer.inc.php';
?>