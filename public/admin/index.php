<?php
/**
 * File index.php
 * Description index file for admin home page
 * @author  Arshdeep Singh Rangi
 */
  $title = "Admin";
  require __DIR__.'/../../config.php';  
  include __DIR__.'/../../includes/header.inc.php';
  include __DIR__.'/../../database/admin_model.php';
  include __DIR__.'/../validate.php';
  //include '../includes/nav.inc.php';

  //checking if the user is logged in as admin or not. if not then send them to the login page.
  if(!isset($_SESSION['admin'])){
  	$_SESSION['not_admin'] = true;
  	$_SESSION['not_admin_message'] = 'Sorry, you need to login as admin first.';
  	header('Location:../login.php');
  	die;
  }
  $relation = getAllRelations($dbh);
  $aggregateValues = getAggregateValues($dbh);
  $numOfDeletedGames = getDeletedCount($dbh);
  
  
?>

			<div id="content">
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
				
				<div id="relation">
					<a class="live_site" href="../index.php">Live Site</a>
					<h2>Tables</h2>
					<ul>
					<?php foreach($relation as $item) : ?>
						<li><a href="table.php?relation=<?=esc_attr($item['Tables_in_gaming_db'])?>"><?=esc_attr($item['Tables_in_gaming_db']) ?></a></li>
					<?php endforeach;?>
					</ul>
				</div>

				<div id="details">
					<h1>Welcome Admin</h1>
					<h2>Some Database Stats</h2>
					<p class="stats">Total Number of Products : <strong><?=esc_attr($aggregateValues['product_count'])?></strong></p>
					<p class="stats">Maximum Price of Title : <strong>$<?=esc_attr($aggregateValues['max_price'])?></strong></p>
					<p class="stats">Minimum Price of Title : <strong>$<?=esc_attr($aggregateValues['min_price'])?></strong></p>
					<p class="stats">Average Price of Titles : <strong>$<?=esc_attr($aggregateValues['avg_price'])?></strong></p>
					<p class="stats">Total Number of Deleted Products : <strong><?=esc_attr($numOfDeletedGames['delete_count'])?></strong></p>
				</div>

			</div>
			
		</div>

<?php
  include '../../includes/footer.inc.php';
?>