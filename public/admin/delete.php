<?php
/**
 * File delete.php
 * Description php file to delete record by its product id
 * @author  Arshdeep Singh Rangi
 */
  $title = "Add Product";
  require __DIR__.'/../../config.php';  
  include __DIR__.'/../../database/admin_model.php';

  //checking if the user is logged in as admin or not. if not then send them to the login page.
  if(!isset($_SESSION['admin'])){
  	$_SESSION['not_admin'] = true;
  	$_SESSION['not_admin_message'] = 'Sorry, you need to login as admin first.';
  	header('Location:../login.php');
  	die;
  }
  
 	//checking if the server request method is GET.
	if($_SERVER['REQUEST_METHOD'] == 'GET'){
		//checking if the product_id is ser in the url and not empty
		if(isset($_GET['product_id']) && !empty($_GET['product_id'])){
			
			//fetching all details of the product, which is to be deleted.
			$product_deleted = getProductRecord($dbh, $_GET['product_id']);
			
			//inserting the product to the archive table before deleting.
			$is_inserted = insertDeletedProduct($dbh, $product_deleted);

			//checking if the product is inserted
			if($is_inserted){
				//if the product is inserted to the archiv table then, delete it.
				$is_deleted = deleteProduct($dbh, $_GET['product_id']);
			}
					
			//if product is deleted successfully then setting the session variable.
			if($is_deleted){
				$_SESSION['delete_product'] = true;
				$_SESSION['delete_message'] = "Product deleted Successfully";
				header('Location: table.php?relation=product');
	    	die;
			}
			//if product is not deleted successfully
			else{
				$_SESSION['delete_product'] = true;
				$_SESSION['delete_message'] = "Product cannot be deleted.";
				header('Location: table.php?relation=product');
	    	die;
			}

		}
	}
	else{
		die('Sorry, you are in wrong place');
	}
