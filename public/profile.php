<?php
  /**File name: profile.php
   * Author: Arshdeep singh
   * Date: 18-08-2018
   * Description: php file for user profile and displaying all the information stored in the users database table.
   */
  $title = "Profile";
  $heading = "Create your Account";
  require __DIR__.'/../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require 'validate.php';
  
  //checking if user is logged in or not
  if(isset($_SESSION['logged_in'])){
    if(isset($_SESSION['msg'])) {
      $message = $_SESSION['msg'];
      unset($_SESSION['msg']);
    }

    if(isset($_SESSION['admin_message'])){
      $message = $_SESSION['admin_message'];
      unset($_SESSION['admin_message']);
    }
    $id=$_SESSION['user_id'];
    //connect to mysql using database handler.
    $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
      //setting database handler to show errors.
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $query = "SELECT first_name, last_name, street, city, postal_code, province, country, telephone, email_id, user_name from user WHERE user_id = :id";
  
    //prepare query
    $stmt = $dbh->prepare($query);
  
    //bind value to the variables used in the query.
    $stmt->bindValue(':id', $id, PDO::PARAM_INT);
  
    //execute query
    $stmt->execute();
  
    //fetch results of the query in this variable.
    $user = $stmt->fetch(PDO::FETCH_ASSOC);
    
  }
  

?>
  
  <div id="content">
      
      <?php if(isset($message)): ?>
        <div id="flash_message2">
          <?=$message;?>
        </div>
      <?php endif; ?>
      <h1 style="text-align: center">User Profile</h1>
    
    <div id="profile">
      <img src="images/profile1.svg" />
      <h2>Welcome <?=esc_attr($user['first_name']);?></h2>
      <div id="personal_info">
        <table>
          <caption>Personal Details</caption>
          <tr>
            <td>First name</td>
            <td><?=esc_attr($user['first_name']);?></td>
          </tr>
          <tr>
            <td>Last name</td>
            <td><?=esc_attr($user['last_name']);?></td>
          </tr>
          <tr>
            <td>User name</td>
            <td><?=esc_attr($user['user_name']);?></td>
          </tr>
          <tr>
            <td>Email Address</td>
            <td><?=esc_attr($user['email_id']);?></td>
          </tr>
          <tr>
            <td>Contact No.</td>
            <td><?=esc_attr($user['telephone']);?></td>
          </tr>
        </table>
      </div>
      
      <div id="address_info">
        <table>
          <caption>Address Details</caption>
          <tr>
            <td>Street</td>
            <td><?=esc_attr($user['street']);?></td>
          </tr>
          <tr>
            <td>City</td>
            <td><?=esc_attr($user['city']);?></td>
          </tr>
          <tr>
            <td>Province</td>
            <td><?=esc_attr($user['province']);?></td>
          </tr>
          <tr>
            <td>Postal Code</td>
            <td><?=esc_attr($user['postal_code']);?></td>
          </tr>
          <tr>
            <td>Country</td>
            <td><?=esc_attr($user['country']);?></td>
          </tr>

        </table>
      </div>
      <hr />
      <p id="profile_buttons">
        <a href="logout.php"><span>Logout</span></a>
        <a href="edit_profile.php"><span>Edit Profile</span></a>
      </p>
    </div>
    
  </div>
  </div>
<?php
  include '../includes/footer.inc.php';
?>