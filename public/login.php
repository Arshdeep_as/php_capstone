<?php
  /**
   *File name: login.php
   * Author: Arshdeep singh
   * Date: 18-08-2018
   * Description: php file for user login and maintaining user session for login and logout
   */
  $title = "Login";
  $heading = "Create your Account";
  require __DIR__.'/../config.php';
  //validator class, storing all the validation functions.
  use \Classes\Utility\Validator;
  $validate = new Classes\Utility\Validator();
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
  require 'validate.php';
  //set empty empty errors array
  $errors = [];
  //check if session['logout'] is set
  if(isset($_SESSION['logout'])) {
    //check if logout message is set.
    if (isset($_SESSION['logout_msg'])) {
      $message = $_SESSION['logout_msg'];
      //unset the logout message.
      unset($_SESSION['logout_msg']);
    }
  }

  //check if session['not_admin'] is set
  if(isset($_SESSION['not_admin'])) {
    //check if not_admin message is set.
    if (isset($_SESSION['not_admin_message'])) {
      $message = $_SESSION['not_admin_message'];
      //unset the not_admin message.
      unset($_SESSION['not_admin_message']);
    }
  }

  //Check if the server request method id POST
  if($_SERVER['REQUEST_METHOD'] == 'POST'){
    //checking if the predefined csrf matches the csrf provided by the post request.
    if($_POST['site_csrf'] == $_SESSION['site_csrf']){
    
      $required_form_fields = ['username', 'password'];
      $validate->required($required_form_fields);
      
      //var_dump($validate->errors());
      //if the user has filled both the fields correctly then we must query the database for correct login credentials.
      if(count($validate->errors()) == 0){
        //connection to database.
        //connect to mysql using database handler.
        $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
        //setting database handler to show errors.
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
        //create query to fetch password from the database where the entered username matches entered email or username.
        $query = "SELECT password FROM user WHERE email_id = :username OR user_name = :username";
    
        //prepare query
        $stmt = $dbh->prepare($query);
    
        //bind values to the variable we used in the query.
        $stmt->bindValue(':username', $_POST['username'], PDO::PARAM_STR);
        
        //execute query
        if($stmt->execute()){
          
          $results = $stmt->fetch(PDO::FETCH_ASSOC);
          if(password_verify($_POST['password'], $results['password'])){
            echo "You are logged in";
            //create query to fetch user_id and user_name from the database of the user.
            $query = "SELECT user_id, user_name, admin FROM user WHERE email_id = :username OR user_name = :username";
            //prepare query
            $stmt = $dbh->prepare($query);
            //bind values to the variable we used in the query.
            $stmt->bindValue(':username', $_POST['username'], PDO::PARAM_STR);
            
            $stmt->execute();
            
            $user_details = $stmt->fetch(PDO::FETCH_ASSOC);
            
            //checking if the user is admin or not
            if($user_details['admin'] == '1'){
              $_SESSION['admin'] = 'true';
              $_SESSION['admin_message']= 'Admin Logged in, Welcome!';
            }
            $_SESSION['user_id'] = $user_details['user_id'];
            $_SESSION['user_name'] = $user_details['user_name'];
            $_SESSION['msg']= 'Congratulations for login!';
            $_SESSION['logged_in'] = true;
            session_regenerate_id();
            
            header('Location: profile.php');
            die;
            //var_dump($_SESSION);
          }
          else {
            //if the execute query for insert data doesn't work, this means data is not inserted in the database.
            $message = "Sorry, credentials do not match our records";
            //die('There was problem in inserting the record');
          }
          //var_dump($results);
        }
        
        
      } //end if count the errors.
      
      $errors = $validate->errors();
    }//end if csrf checking
    else{
      die('Sorry, Nice try but you failed.');
    }  
  }//end if the server method is post.
  
?>
  
  <div id="content">
    <!--[if LTE IE 8]>
    <h2>Hey, To get the best experience of this website, Please update your browser!</h2>
    <![endif]-->
    <div id="login_container">
      <h1>Login</h1>
      <?php if(isset($message)) : ?>
        <div id="flash_message">
          <?=$message;?>
        </div>
      <?php endif;?>
      <form method="post" action="" novalidate>
        <!-- Setting up the csrf topken in the hidden field-->
        <input type="hidden" name="site_csrf" value="<?=$_SESSION['site_csrf'];?>" />
        <p><input type="text" id="username" name="username" placeholder="username or email-Id" value="<?php if(!empty($_POST['username'])){ echo esc_attr($_POST['username']); } ?>" /></p>
        <?php if(!empty($errors['username'])) :?>
          <span class="show_error"><small><?=esc($errors['username']);?></small></span>
          <script>document.getElementById('username').style.backgroundColor="#f33";</script>
        <?php endif; ?>
        <p><input type="password" id="password" name="password" placeholder="your password" /></p>
        <?php if(!empty($errors['password'])) :?>
          <span class="show_error"><small><?=esc($errors['password']);?></small></span>
          <script>document.getElementById('password').style.backgroundColor="#f33";</script>
        <?php endif; ?>
        <p><button type="submit">Login</button><button type="reset">Clear</button></p>
      </form>
      <p>Don't have an account yet, <a href="sign_up.php">Sign Up</a> here.</p>
    </div>
    
  
  </div>
</div>
<?php
  include '../includes/footer.inc.php';
?>