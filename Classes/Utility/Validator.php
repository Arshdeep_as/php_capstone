<?php
  /**
   * Created by PhpStorm.
   * User: Arshdeep singh
   * Date: 16-08-2018
   * Time: 06:32 PM
   */
  
  namespace Classes\Utility;
  
  class Validator {
    // errors array to store the errors which may came while validating the form data.
    private $errors = [];
    
    /**
     * Validate required fields in $_POST.
     * @param array $field_name - name of field to validate
     * @return void.
     */
    
    
    public function required($required_elements)
    {
      foreach($required_elements as $value){
        $original_name = ucfirst(str_replace('_', ' ', $value));
        if(empty($_POST[$value])){
          $this->errors[$value] = "{$original_name} is a required field.";
        }
      }
    }
  
    /**
     * The purpose of this function is to validate the user email address.
     * @param $field_name string, contains the email address of the user.
     * @return void.
     */
    public function emailValidation($field_name)
    {
      if(empty($_POST[$field_name])){
        $this->errors[$field_name] = "Email is a required field.";
      }
      else if(!filter_input(INPUT_POST, $field_name, FILTER_VALIDATE_EMAIL)) {
        $this->errors[$field_name] = "Please enter a valid email address";
      }
    }
  
    /**
     * @param $character_fields array, contains the list of field names which can only contain characters in their values.
     */
    public function onlyCharacters($character_fields)
    {
      foreach ($character_fields as $value) {
        if(!empty($_POST[$value])){
          $original_name = ucfirst(str_replace('_', ' ', $value));
          if (!preg_match("/^[a-zA-Z\s]+$/", $_POST[$value])) {
            $this->errors[$value] = "{$original_name} can only contains alphabets and whitespaces";
          }
        }
      }
    }
  
    /**
     * @param $field_name number or varchar, stores the telephone number user just enters then match it against regex, to validate the
     * user phone number.
     */
    public function telephoneValidation($field_name)
    {
      if(!empty($_POST[$field_name])){
        if(preg_match("/^[A-z]+$/",$_POST[$field_name])){
          $this->errors[$field_name] = "{$field_name} cannot contains alphabets. ";
        }
        else if(!preg_match("/^(\(?[0-9]{3}\)?)-?\s?([0-9]{3})-?\s?([0-9]{4})$/",$_POST[$field_name])){
          $this->errors[$field_name] = "{$field_name} format is incorrect.(xxx-xxx-xxxx | (xxx)xxx-xxx | xxx xxx xxxx) ";
        }
      }
    }
  
    /**
     * @param $field_name string or varchar, stores the postal code user just entered. match the postal code against the canadian
     * postal code rules.
     */
    public function postalCodeValidation($field_name)
    {
      if(!empty($_POST[$field_name])){
        $original_name = ucfirst(str_replace('_', ' ', $field_name));
        if(!preg_match("/^[ABCEGHJKLMNPRSTVXY][0-9][ABCEGHJKLMNPRSTVWXYZ]\s?[0-9][ABCEGHJKLMNPRSTVWXYZ][0-9]$/",$_POST[$field_name])){
          $this->errors[$field_name] = "{$original_name} format is incorrect (A1A 1A1) Uppercase Only. ";
        }
      }
    }
  
    /**
     * @param $field_name String or varchar the password user just entered.
     * @param $compare_field string or varchar the confirm password the user just entered. compare the password against predefined set
     * of rules to determine the password strength.
     */
    public function passwordValidation($field_name, $compare_field)
    {
      if(!empty($_POST[$field_name])){
        if($_POST[$field_name] != $_POST[$compare_field]){
          $this->errors[$field_name] = "Password and Confirm Password are not same.";
        }
        else if(strlen($_POST[$field_name]) < 8){
          $this->errors[$field_name] = "Password must be atleast 8 characters long.";
        }
        else if(!preg_match('/(?=.*[A-Z])/', $_POST[$field_name])){
          $this->errors[$field_name] = "Password must contains Uppercase Characters.";
        }
        else if(!preg_match('/(?=.*[a-z])/', $_POST[$field_name])){
          $this->errors[$field_name] = "Password must contains Lowercase Characters.";
        }
        else if(!preg_match('/(?=.*[0-9])/', $_POST[$field_name])){
          $this->errors[$field_name] = "Password must contain digits.";
        }
        else if(!preg_match('/(?=.*[[:punct:]])/', $_POST[$field_name])){
          $this->errors[$field_name] = "Password must contain special characters.";
        }
        
      }
    }
  
    /**
     * @param $street_name string or varchar, name of the street which user just entered.
     */
    public function streetNameValidation($street_name)
    {
      if(!empty($_POST[$street_name])){
        if(!preg_match('/^[A-z0-9\s\-]+$/', $_POST[$street_name])){
          $this->errors[$street_name] = "Entered Street name is not valid";
        }
      }
    }
  
    /**
     * username and email unique validation
     */
    public function checkForUsername($fieldname){
      //connection to database.
      //connect to mysql using database handler.
      
      
      $dbh = new PDO(DB_DSN, DB_USER, DB_PASS);
      //setting database handler to show errors.
      $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  
      //create query to insert records into our database.
      $query = "SELECT email_id, user_name from user WHERE user_emial_id = :username";
  
      //prepare query
      $stmt = $dbh->prepare($query);
  
      //bind value to the variables used in the query.
      $stmt->bindValue(':username', $_POST[$fieldname], PDO::PARAM_INT);
  
      //execute query
      $stmt->execute();
  
      //fetch results of the query in this variable.
      $user = $stmt->fetch(PDO::FETCH_ASSOC);
      if(!empty($user)){
        $this->errors[$fieldname]='Sorry, this username is already taken';
      }
    }
    
    
    /**
     * function to validate the content of any field, only chracters number and space.
     * @param  [string] $field_name name of the field
     * @return none             
     */
    public function nameValidation($field_name)
    {
      if(!empty($_POST[$field_name])){
        if(!preg_match('/^[A-z0-9\s\-]+$/', $_POST[$field_name])){
          $this->errors[$field_name] = ucfirst($field_name)." is not valid input (only A-z, 0-9, space)";
        }
      }
    }

    /**
     * function to validate the content of any field, only chracters number and space.
     * @param  [string] $field_name name of the field
     * @return none 
     */
    public function numberValidation($field_name)
    {
      if(!empty($_POST[$field_name])){
        if(!preg_match('/^[0-9]+$/', $_POST[$field_name])){
          $this->errors[$field_name] = ucfirst($field_name)." can only contain numbers.";
        }
      }
    }

    /**
     * function to validate the content of any field, only floting point number
     * @param  [string] $field_name name of the field
     * @return none 
     */
    public function floatValidation($field_name)
    {
      if(!empty($_POST[$field_name])){
        if(!preg_match('/^[0-9]+(\.+[0-9]{1,2})?$/', $_POST[$field_name])){
          $this->errors[$field_name] = ucfirst($field_name)." can only contain numbers.(decimal upto two places)";
        }
      }
    }

    /**
     * @param none(void) the purpose of this function is to just return the errors array.
     */
    public function errors()
    {
      return $this->errors;
    }
  }
