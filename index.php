<?php
/**
 *Description
 *@filename index.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */
  $title = "Capstone Project";
  $heading = "Best selling Games";
  include '../config.php';
  include '../includes/header.inc.php';
  include '../includes/nav.inc.php';
?>
			
			<main id="content"><!--Content Div Starts-->
				
				<!--[if LTE IE 8]>
					<h2>Hey, To get the best experience of this website, Please update your browser!</h2>
				<![endif]-->
        <h1 style="text-align: center"><?=$heading;?></h1>
				<div id="hero_div">
					<img src="images/hero_image/hero2.jpg" alt="Banner Image"/>
				</div>
				
				<div id="offers_div"><!--Offers Div Starts-->
					<div id="latest_games"><!--Latest games Div Starts-->
						<p class="tag1">Latest Games</p>
						<div class="latest_games_items">
						
							<a href="#"><img src="images/ps4/farcry5.jpg" alt="Farcry5" /></a>
							<p>Farcry 5</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<div class="latest_games_items">
							<!--Image Source https://i.pinimg.com/originals/be/9d/0d/be9d0df86f3e49059cfa1345cf980e15.jpg-->
							<a href="#"><img src="images/ps4/god_of_war.jpg" alt="Farcry5" /></a>
							<p>God of War</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<p><a href="pc_games.html">see all...</a></p>
					</div><!--Latest games Div Ends-->
					
					<div id="pc_games"><!--PC games Div Starts-->
						<p class="tag1">PC Games</p>
						<div class="upcomming_games_items">
							<!--Image source https://i.pinimg.com/originals/00/fa/4e/00fa4e177df49279ac758c50a5487a72.jpg -->
							<a href="#"><img src="images/pc/farcry5.jpg" alt="Farcry5" /></a>
							<p>Farcry 5</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<div class="upcomming_games_items">
							<!--Image source http://i60.tinypic.com/qsmlbl.jpg -->
							<a href="#"><img src="images/pc/metro.jpg" alt="Farcry5" /></a>
							<p>Metro</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<p><a href="pc_games.html">see all...</a></p>
					</div><!--PC games Div Ends-->
					
					<div id="ps4_games"><!--PS4 games Div Starts-->
						<p class="tag1">Play Station 4 Games</p>
						<div class="latest_games_items">
							<a href="#"><img src="images/ps4/uncharted1.jpg" alt="Farcry5" /></a>
							<p>Uncharted 4</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<div class="latest_games_items">
							<!--Image Source https://vignette.wikia.nocookie.net/nfs/images/d/d1/NFSPB_Boxart_Deluxe.jpg/revision/latest?cb=20170728112006&path-prefix=en-->
							<a href="#"><img src="images/ps4/nfs_payback.jpg" alt="Farcry5" /></a>
							<p>Payback</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<p><a href="pc_games.html">see all...</a></p>
					</div><!--PS4 games Div ends-->
					
					<div id="xbox_games"><!--Xbox1 games Div Starts-->
						<p class="tag1">Xbox 1 Games</p>
						<div class="upcomming_games_items">
							<a href="#"><img src="images/xbox/Zombie-Armies.jpg" alt="Farcry5" /></a>
							<p>Zombie Arival</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<div class="upcomming_games_items">
							<!--Image Source http://www.mobygames.com/images/covers/l/416758-fortnite-standard-founder-s-pack-xbox-one-front-cover.png-->
							<a href="#"><img src="images/xbox/fortnite.jpg" alt="Farcry5" /></a>
							<p>Fortnite</p>
							<p>$98.00</p>
							<div class="button"><a href="sorry_message.html"><span>Buy Now</span></a></div>
						</div>
						<p><a href="pc_games.html">see all...</a></p>
					</div>	<!--Xbox1 games Div ends-->				
				</div><!--Offers Div Ends-->
				
				<div id="callout"><!--Callout Div starts-->
					<p>Don't have an account yet...?&nbsp;&nbsp;&nbsp;Create your account Now...</p>
					<div class="button"><a href="sign_up.html"><span>Sign up</span></a></div>
				</div><!--Callout Div ends-->
				
			</main><!--Content Div Ends-->
			<div class="clearfix"></div>
		</div><!--Wrapper Div Starts-->
		
<?php
  include '../includes/footer.inc.php';
?>