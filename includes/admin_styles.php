<style>
	
	body{
		background-color: #e9ece5;
	}
	#wrapper{
		width: 100%;

	}
	#relation{
		width: 170px;
		padding: 20px;
		display: inline-block;
		float: left;
		/*background-color: #aaa;*/
		min-height: 1000px;
	}
	#relation .live_site{
		display: inline-block;
		text-decoration: none;
		color: #000;
		border: 1px solid #000;
		text-align: center;
		width: 100%;
		padding: 5px;
		background-color: #f33;
	}
	#relation .live_site:hover{
		background-color: #3f3;
	}
	#relation ul{
		/*width: 100%;*/
		list-style-type: none;
		padding-left: 0px;
	}
	#relation ul li{
		/*width: 100%;*/
		padding: 5px;
	}
	#relation ul li a{
		display: inline-block;
		text-decoration: none;
		color: #000;
		border: 1px solid #000;
		text-align: center;
		width: 100%;
		padding: 5px;
	}
	#relation ul li a:hover{
		background-color: #999;
	}
	#details{
		width: 74%;
		margin: 0 auto;
		display: inline-block;
		float: left;
		padding: 30px; 
	}

	h1, h2{
		text-align: center;
	}

	#table_container{
		/*background-color: #fcc;*/
		width: 100%;
		min-height: 600px;
		margin-left: 20px;
	}
	#clearfix{
		clear: both;
	}
	#detail_form{
		width:80%;
		margin: 0 auto;
	}
	#content form{
		width: 50%;
	}
	#details p.stats{
		margin: 30px;
		padding: 10px;
		/*background-color: #4d4;*/

	}

	#flash_message_admin{
		background-color: #3f3;
		opacity: 0.5;
		height: 50px;
		width: 40%;
		margin: 0 auto;
		border-radius: 5px;
		padding-top: 5%;
		padding-bottom: 3%;
		text-align: center;
		vertical-align: middle;
	}

	#add_product{

	}
	#add_product p{
		text-align: center;
	}
	#add_product p a{
		text-decoration: none;
		padding: 20px;
		background-color: #6f6;
		display: inline-block;
		color: #000;
		border-radius: 10px;
	}
	#add_product p a:hover{
		background-color: #3f3;
	}
	#details table tr td a{
		text-decoration: none;
		padding: 5px;		
		display: inline-block;
		width: 70px;
		color: #000;
		border-radius: 10px;
	}
	#details table tr td a#red{
		background-color: #f66;

	}
	#details table tr td a#red:hover{
		background-color: #f33;

	}
	#details table tr td a#green{
		background-color: #6f6;
		margin-bottom: 10px;
	}
	#details table tr td a#green:hover{
		background-color: #3f3;
	}

	#detail_form{
		padding: 40px;
	}

	#detail_form form{
		width: 50%;
		margin: 0 auto;
	}
	#detail_form form p{
		text-align: center;
	}
	#detail_form form label{
		width: 170px;
		display: inline-block;
		text-align: center;
	}
	#detail_form form input[type=text]{
		width: 200px;
		border: none;
		height: 30px;
		padding: 5px;
		text-align: center;
	}
	#detail_form form select{
		width: 200px;
		border: none;
		height: 35px;
		padding: 6px;
		text-align: center;
	}
	#detail_form form input[type=text]:hover{
		background-color: #3f3;
	}
	#detail_form form select:hover{
		background-color: #3f3;
	}
	#detail_form form .admin_buttons{
		margin-top: 20px;
		width: 200px;
		border: 0;
		height: 30px;
		background-color: #7f7;
		color: #000;
		border-radius: 10px;
	}
	#detail_form form .admin_buttons:hover{
		background-color: #2f2;
	}
</style>