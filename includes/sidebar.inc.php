<?php
  /**
   *Description
   *@filename sidebar.inc.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
?><div id="filter_box">
  <div id="form_div">
    <form id="filters"
          method="post"
          action="http://www.scott-media.com/test/form_display.php"
          name="filters"
    >
      
      <p>Genre :</p>
      
      <input type="checkbox" name="genre" id="action" value="action" />
      <label for="action"> Action</label><br />
      
      <input type="checkbox" name="genre_1" id="adventure" value="adventure" />
      <label for="adventure"> Adventure</label><br />
      
      <input type="checkbox" name="genre_2" id="racing" value="racing" />
      <label for="racing"> Racing</label><br />
      
      <input type="checkbox" name="genre_3" id="puzzle" value="puzzle" />
      <label for="puzzle"> Puzzle</label><br />
      
      <input type="checkbox" name="genre_4" id="rpg" value="rpg" />
      <label for="rpg"> RPG</label><br />
      
      <input type="checkbox" name="genre_5" id="shooting" value="shooting" />
      <label for="shooting"> Shooting</label><br />
      
      <input type="checkbox" name="genre_6" id="strategy" value="strategy" />
      <label for="strategy"> Strategy</label><br />
      
      <input type="checkbox" name="genre_7" id="sports" value="sports" />
      <label for="sports"> Sports</label><br />
      
      <p>Platform :</p>
      
      <input type="checkbox" name="platform_1" id="pc" value="pc" />
      <label for="pc"> PC</label><br />
      
      <input type="checkbox" name="platform_2" id="xbox_one" value="xbox_one" />
      <label for="xbox_one"> Xbox One</label><br />
      
      <input type="checkbox" name="platform_3" id="playstation_4" value="playstation_4" />
      <label for="playstation_4"> Playstation 4</label><br />
      
      <input type="checkbox" name="platform_4" id="switch" value="switch" />
      <label for="switch"> Nitendo Switch</label><br />
      
      <input type="checkbox" name="platform_5" id="other" value="other" />
      <label for="other"> Others</label><br />
      
      <p>Price :</p>
      
      <input type="radio" name="price" id="under_20" value="under_20" />
      <label for="under_20"> under $30</label><br />
      
      <input type="radio" name="price" id="30-50" value="30-50" />
      <label for="30-50"> between $30 and $50</label><br />
      
      <input type="radio" name="price" id="50-70" value="50-70" />
      <label for="50-70"> between $50 and $70</label><br />
      
      <input type="radio" name="price" id="above_70" value="above_70" />
      <label for="above_70"> above $70</label><br /><br />
      
      <p>
        <input type="submit" value="Find Now" id="send_form"> &nbsp;&nbsp;
      </p>
    </form>
  </div>
</div>