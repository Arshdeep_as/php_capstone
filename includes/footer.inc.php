<?php
  /**
   *Description
   *@filename footer.inc.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
?><div id="container_footer"><!--Footer container div Starts-->
  <footer>
    <div id="footer_box">
      <div id="col1"><!--Footer container div Starts-->
        <h2><a href="index.php">Home</a></h2>
      </div>
      <div id="col2">
        <h2>Games</h2>
        <ul>
          <li><a href="pc_games.php">PC Games</a></li>
          <li><a href="xbox1_games.php">Xbox One</a></li>
          <li><a href="ps4_games.php">Playstation 4</a></li>
        </ul>
      </div>
      <div id="col3">
        <h2>Information</h2>
        <ul>
          <li><a href="about_us.php">About Us</a></li>
          <li><a href="about_us.php">Contact Us</a></li>
          <li><a href="sign_up.php">Create Account</a></li>
          <li><a href="sorry_message.php">Privacy</a></li>
        </ul>
      </div>
      <div id="col4">
        <h2>Follow us</h2>
        <div id="social_icons">
          <div class="icons"><a href="https://www.facebook.com"><img src="images/facebooks_icon.svg" alt="facebook"/></a></div>
          <div class="icons"><a href="https://www.instagram.com"><img src="images/instagram_icon.svg" alt="instagram"/></a></div>
          <div class="icons"><a href="https://www.twitter.com"><img src="images/twitter_icon.svg" alt="twitter"/></a></div>
          <div class="icons"><a href="https://www.linkedin.com"><img src="images/linkedin_icon.svg" alt="linkedin"/></a></div>
        </div>
      </div>
      <div class="clearfix"></div>
      <p id="copyright">
        Copyright © 2018 - Arshdeep Singh Rangi<br />
      </p>
    </div>
    
    
  </footer>
</div><!--Footer container div ends-->
</body>
</html>