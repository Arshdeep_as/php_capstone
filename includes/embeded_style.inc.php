<style>
		#registrationForm{
			background-color: #e9ece5;
		}
		input[type=text],
		input[type=password],
		input[type=email],
		input[type=tel]{
			background-color: #e9ece5;
      border-bottom: 2px solid #3b3a36;
		}
		#registrationForm #formButton .sign_up_formbutton{
			background-color: #3b3a36;
      color: #e9ece5;
		}
		#registrationForm #formButton .sign_up_formbutton:hover{
			background-color: #000;
			color:#fff;
		}
	</style>