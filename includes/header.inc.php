<?php
/**
 *Description
 *@filename header.inc.php
 *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
 *@created_at 2018-08-02
 */

?><!doctype html>

<!--
   _____               .__         .___                       _________.__               .__
  /  _  \_______  _____|  |__    __| _/____   ____ ______    /   _____/|__| ____    ____ |  |__
 /  /_\  \_  __ \/  ___/  |  \  / __ |/ __ \_/ __ \\____ \   \_____  \ |  |/    \  / ___\|  |  \
/    |    \  | \/\___ \|   Y  \/ /_/ \  ___/\  ___/|  |_> >  /        \|  |   |  \/ /_/  >   Y  \
\____|__  /__|  /____  >___|  /\____ |\___  >\___  >   __/  /_______  /|__|___|  /\___  /|___|  /
        \/           \/     \/      \/    \/     \/|__|             \/         \//_____/      \/
-->

<html lang="en">
<head>
  <title><?=$title;?></title>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link href="https://fonts.googleapis.com/css?family=Raleway:400" rel="stylesheet">
  
  <script src="/scripts/myscript.js"></script>
  <link rel="stylesheet"
        type="text/css"
        href="styles/desktop_stylesheet.css"
        media="only screen and (min-width: 769px)" />
  <link rel="stylesheet"
        type="text/css"
        href="styles/mobile_stylesheet.css"
        media="only screen and (max-width: 768px)" />
  <link rel="stylesheet"
        type="text/css"
        href="styles/print_stylesheet.css"
        media="print" />
  <link rel="shortcut icon" href="images/fav_icon/favicon.ico" type="image/x-icon">
  <link rel="icon" href="images/fav_icon/favicon.ico" type="image/x-icon">
  <?php if($title == "Sign Up")
        { include('embeded_style.inc.php');
        }
        ?>

  <?php if($title == "Admin" || $title == "Edit Product" || $title == "Add Product") :?>
    <link rel="stylesheet"
        type="text/css"
        href="../styles/desktop_stylesheet.css"
        media="only screen and (min-width: 769px)" />
     <?php include 'admin_styles.php' ?>   
  <?php endif; ?>



  <!--[if LTE IE 8]>
  <style>
    body{
      font-family: 'Raleway', Arial, Helvetica, sans-serif;
      font-size: 16px;
      line-height: 20px;
    }
    /*css style for main page*/
    #wrapper{
      background-color: #fff;
      width: 960px;
      margin: 0 auto;
    }
  
  </style>
  
  <script>
    document.createElement('header');
    document.createElement('nav');
    document.createElement('footer');
    document.createElement('article');
    document.createElement('section');
    document.createElement('main');
  </script>
  <![endif]-->

  <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
  
  <script>
    $(document).ready(function(){
      getPressedKey();
    });

    function getPressedKey(){
      $('#search_bar input').keyup(function(e){
        e.preventDefault();
        var data = {};
        data.search = $('#search').val();
        console.log(data.search);
        getTitleDetails(data);
      });
    }

    function getTitleDetails(data){
      $.post('server/search_form.php', data, function(response){
        console.log(response);
        showGamesList(response);
      });
    }

    function showGamesList(data){
      var content = '<ul>';
      for(var i=0; i<data.length; i++){
        //console.log(data[i].book_id);
        content += '<li data-id="'+ data[i].product_id +'">'+ data[i].name +'</li>';
      }
      content +='</ul>';
      $('#list').html(content);

      handleClicks();
    }

    function handleClicks(){
      $('#list li').each(function(){
        $(this).click(function(){
          console.log($(this).attr('data-id'));
          var name = $(this).text();
          console.log(name);
          location.href = 'product_details.php?search='+$(this).text();
        });
      });
    }
  </script>

</head>

<body>
<div id="container_header">
  <header>
    <div id="logo"></div>
    <div id="tagline">
      <!--<h1>Lets Start Playing</h1>-->
    </div>
    <div id="utility_nav">

      <?php if($title != "Admin") : ?>
        <?php if(!isset($_SESSION['user_id'])) : ?>
          <ul>
            <li><a href="login.php">Login</a></li>
            <li><a id="special" href="sign_up.php">Sign up</a></li>
          </ul>
        <?php endif; ?>
        <?php if(isset($_SESSION['user_id'])) : ?>
          <ul>
            <li><a href="profile.php">Profile</a></li>
            <li><a href="checkout.php">Checkout</a></li>
            <?php if(isset($_SESSION['admin'])) : ?>
              <li><a href="admin/index.php">Admin</a></li>
            <?php endif; ?>
            <li><a href="logout.php">Logout</a></li>
          </ul>
        <?php endif; ?>
      <?php endif; ?>

    </div>
    <?php if($title == "Admin" || $title == "Edit Product" || $title == "Add Product") : ?>
      <div id="search_div">
        <form id="search_bar" method="get" action="table.php" name="filters" autocomplete="off" novalidate="novalidate">
          <input type="text" id="search" name="search" placeholder=" Search...."/>
          <button type="submit" id="search_button">&#128269;</button>
          <!--<p><a href="sign_up.php">Sign up</a><a href="login.php">Login</a> </p>-->
          <div id="list">

          </div>
        </form>
      </div>
    <?php else : ?>
      <div id="search_div">
        <form id="search_bar" method="get" action="product_details.php" name="filters" autocomplete="off" novalidate="novalidate">
          <input type="text" id="search" name="search" placeholder=" Search...."/>
          <button type="submit" id="search_button">&#128269;</button>
          <!--<p><a href="sign_up.php">Sign up</a><a href="login.php">Login</a> </p>-->
          <div id="list">

          </div>
        </form>
      </div>
    <?php endif; ?>
    
  </header>
</div>
<div id="wrapper"><!--Wrapper Div Starts-->
