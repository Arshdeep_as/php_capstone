<?php
  /**
   *Description
   *@filename nav.inc.php
   *@author Arshdeep Singh <arshdeep6445.as@gmail.com>
   *@created_at 2018-08-02
   */
?><nav><!--Navigation Starts-->
  <div id="menu">
    <!-- Hamburger Menu Button -->
    <a href="#" id="menulink">
      <span id="hamburger_top"></span>
      <span id="hamburger_middle"></span>
      <span id="hamburger_bottom"></span>
    </a>
    <!-- Hamburger Menu Button Ends -->
    <ul id="main_menu"><!--Main NAvigation List-->
      <li><a <?php if($title == "Capstone Project"){ echo 'class="i_am_here"';}?> href="index.php" title="Home">Home</a>
      </li><li class="submenu3"><a <?php if(($title == "PC Games")||($title == "Xbox One Games")||($title == "PS4 Games")){ echo 'class="i_am_here"';}?> href="#">Games</a>
        <ul class="itemlist">
          <li><a <?php if($title == "PC Games"){ echo 'class="i_am_here"';}?> href="pc_games.php" title="PC Games">PC Games</a></li>
          <li><a <?php if($title == "Xbox One Games"){ echo 'class="i_am_here"';}?> href="xbox1_games.php" title="Xbox Games">Xbox 360</a></li>
          <li><a <?php if($title == "PS4 Games"){ echo 'class="i_am_here"';}?> href="ps4_games.php" title="PS4 Games">Play Station 4</a></li>
        </ul>
      </li><li><a <?php if($title == "Best Offers"){ echo 'class="i_am_here"';}?> href="best_offers.php" title="Best Offers">Best Offers</a>
        <?php if(!isset($_SESSION['user_id'])) : ?>
      </li><li><a <?php if($title == "Sign Up"){ echo 'class="i_am_here"';}?> href="sign_up.php" title="Sign up">Sign Up</a>
    
        <?php endif; ?>
        <?php if(isset($_SESSION['user_id'])) : ?>
      </li><li><a <?php if($title == "Profile"){ echo 'class="i_am_here"';}?> href="profile.php" title="Profile">Profile</a>
        <?php endif; ?>
      </li><li><a <?php if($title == "About Us"){ echo 'class="i_am_here"';}?> href="about_us.php"  title="About us">About Us</a></li>
    </ul><!--Main NAvigation List ends-->
  </div><!--Main div menu ends-->
</nav><!--Navigation Ends-->