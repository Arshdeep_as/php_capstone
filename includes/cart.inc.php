<div class="cart">


<?php 
   if(!empty($_SESSION['cart'])){


    
    
    $details=[];
  
    $items= $_SESSION['cart'];
    foreach($items AS $row){
      $details[]=$row;
    }


  }

?>
  
  <?php if(!empty($_SESSION['cart'])): ?>
    <div id="cart">
      <strong>Your Cart:</strong><br />

      <small>You have <?=count($details)?> item on your cart.</small><br />
      <table id="cart_table">
        <tr>
          <th>Name</th>
          <th>Platform</th>
          <th>Remove</th>
        </tr>
        <?php foreach($details AS $row) : ?>
          <tr>
            <td><?=$row['name']?></td>
            <td><?=$row['platform']?></td>
            <td><span class="remove"> &nbsp;&nbsp;<a href="removegame.php?id=<?=$row['product_id']?>" >X</a></span></td>
          </tr>  
        <?php endforeach; ?>
      </table>
      
      <small><a id="checkout" href="checkout.php">Checkout now!</a></small>
    </div>
  <?php endif; ?>

</div>